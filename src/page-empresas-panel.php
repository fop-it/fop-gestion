<?php get_header(); ?>

include( 'inc/fop-utils.php' );


<div id="page-content" class="page-content">
  <div>
    <div style="height:140px" aria-hidden="true" class="wp-block-spacer"></div>

    <?php if( isset ( $_GET[ 'panel' ] ) ) : ?>

    <table class="blueTable" style="width:400px" class="center">
      <thead>
        <tr>
          <th>CUIT</th>
          <th>Razón Social</th>
          <th>Sector</th>
          <th>Estrato</th>
          <th>CIIU</th>
          <th>Empleados</th>
          <th>Ranking</th>
          <th>Región</th>
          <th>Provincia</th>
          <th>Teléfono</th>
          <th>Email</th>
          <th>Contacto</th>
        </tr>
      </thead>
      <tbody>
      <?php
        global $wpdb;
        $result = $wpdb->get_results( 'SELECT * FROM fop_panels_companies 
                                       WHERE JSON_EXTRACT(companySurvey, "$.panel") = "' . $_GET[ 'panel' ] . '"'  );

        foreach( $result as $row ) : 
          $contact = $wpdb->get_row( 'SELECT * FROM fop_panels_companies_contact 
                                      WHERE companyId = ' . $row->companyId ); ?>
          <tr>
            <td><?php echo $row->companyCUIT; ?></td>
            <td><?php echo $row->companyName; ?></td>
            <td><?php echo $row->companyBranch; ?></td>
            <td><?php echo $row->companyFopStratum; ?></td>
            <td><?php echo $row->companyCIIU; ?></td>
            <td><?php echo $row->companyEmployees; ?></td>
            <td><?php echo JSON_DECODE( $row->companyMarketing ) ->scoring; ?></td>
            <td><?php echo $row->companyRegion; ?></td>
            <td><?php echo $contact->companyProvince; ?></td>
            <td><?php echo JSON_DECODE( $contact->companyContact )->contact->telephone; ?></td>
            <td><?php echo JSON_DECODE( $contact->companyContact )->contact->email_1 . "   " .
                           JSON_DECODE( $contact->companyContact )->contact->email_2 . "   " .
                           JSON_DECODE( $contact->companyContact )->contact->email_3 . "   " .
                           JSON_DECODE( $contact->companyContact )->contact->email_4 . "   " .
                           JSON_DECODE( $contact->companyContact )->contact->email_5 . "   " .
                           JSON_DECODE( $contact->companyContact )->contact->email_6; ?></td>
            <td><?php echo JSON_DECODE( $contact->companyContact )->contact->last_contact ?></td>
          </tr>
     <?php endforeach;?>
      </tbody>
    </table>

    <?php else: ?>

    <div class="fop-form">
      <h5> Visualice las empresas por Panel </h5>

      <br>
      <form action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post">
        <br>
          <?php
            global $wpdb;
            $paneles = $wpdb->get_results( "SELECT panelName FROM fop_panels" );
          ?>

          <select name="txtPanels" style="width: 450px;">
          <option>Seleccione Panel</option>
            <?php
              foreach( $paneles as $panel ) {
                echo '<option value="'.$panel->panelName.'">'.$panel->panelName.'</option>';
              }
            ?>
          </select>

        <br>
        <input type="submit" class="fop-button" value="Ver Empresas" >

        <input type="hidden" name="action" value="viewPanelCompanies">
      </form>
    </div>
    <?php endif; ?>

    <div style="text-align: center; ">
      <br> <br>
      <a href="/universo-de-empresas/">
       <input type="submit" class="fop-button" value="Volver a Empresas">
      </a>
      <br>
    </div>

  </div>
</div>

<?php get_footer(); ?>

