<?php


$path = "/var/www/html/wp-content/uploads/fop/";

$file = $_GET[ 'f' ] . '.xlsx';
$downloadFile = $path . $file;

$filename = $_GET[ 't' ] . '.xlsx';

if ( file_exists( $downloadFile ) ) {
  header( 'Content-Disposition: attachment; filename="' . $filename . '"' );
  header( 'Content-Type: application/vnd.ms-excel' );
  header( 'Content-Length: ' . filesize( $file ) );
  header( 'Cache-Control: must-revalidate' );
  header( 'Pragma: public' );

  readfile( $downloadFile );

  exit;
} else {
  echo "ERROR: No se pudo descargar el archivo " . $downloadFile;
}


?>
