<?php

include_once 'fop-utils.php';
require_once '/var/www/html/wp-content/themes/one-page-express-child/inc/afip-06/src/Afip.php';


function captura_activos_afip() {
  if( $_FILES[ 'afipFile' ][ 'name' ] ) {
    if( !function_exists( 'wp_handle_upload' ) ) {
      require_once( ABSPATH . 'wp-admin/includes/file.php' );
    }

    $afip_active = wp_handle_upload( $_FILES[ 'afipFile' ], array( 'test_form' => false ) );

    if( $afip_active && !isset( $afip_active[ 'error' ] ) ) {
      $success = extract_afip_zip_file( $afip_active[ 'file' ] );

      wp_delete_file( $afip_active[ 'file' ] );
 
      if( $success != false ) {
        wp_redirect( add_query_arg( array( 'msg' => "Archivo listo para procesar",
                                           'f' => $success ),
                                    get_home_url() . '/afip/') );
        exit;
      } else {
        wp_redirect( add_query_arg( array( 'errormsg' => "ERROR: El archivo no pudo ser tratado" ),
                                    get_home_url() . '/afip/') );
        exit;
      }
    } else {
      wp_redirect( add_query_arg( array( 'errormsg' => "ERROR: " . $afip_active[ 'error' ] ),
                                  get_home_url() . '/afip/') );
      exit;
    }
  } else {
    wp_redirect( add_query_arg( array( 'errormsg' => "ERROR: No ha cargado ningún archivo" ),
                                get_home_url() . '/afip/') );
    exit;
  }
}

function reprocesa_cuits_afip() {
  if( empty( $_POST[ 'afipFile' ] ) ):
    wp_redirect( add_query_arg( array( 'errormsg' => "ERROR: No se encuentra el archivo" ),
                                get_home_url() . '/afip/') );
    exit;
  endif;

  global $wpdb;

  $path = '/var/www/html/wp-content/uploads/fop/afip/';
  $full_file = $path . 'utlfile/padr/' . $_POST[ 'afipFile' ];
  $handler = fopen( $full_file, "r" );

  if( $handler ) {
    $employeers = $_POST[ 'emp' ];
    $solo       = $_POST[ 'solo' ];
    $not_found  = $_POST[ 'nfound' ];

    $line = fgets( $handler );

    while( substr( $line, 0, strlen( $_POST[ 'lastCuit' ] ) ) !== $_POST[ 'lastCuit' ] ) {
      $line = fgets( $handler );
    }

    while( !feof( $handler ) ) {
      if( $line[ 18 ] == 'S' ) {
        // Empleador
        $employeers++;

        $cuit = array( substr( $line, 0, 11 ) );
        if( insert_afipdb_active( $cuit[0] ) == false ) {
          //Empleador: AFIP retorna ERROR
          $not_found++;

          $wpdb->insert( 'fop_afip_errors', 
                         array( 'errorCUIT' => $cuit[0],
                                'errorDate' => current_time( 'mysql', 1 ) ) ); 
          if( $not_found > 2 ) {
            fclose( $handler );
            wp_redirect( add_query_arg( array( 'afip' => "Errores en el Servidor AFIP", 
                                               'f' => $_POST[ 'afipFile' ],
                                               'lc' => $cuit[0],
                                               'emp' => $employeers,
                                               'solo' => $solo,
                                               'nfound' => $not_found ),
                                        get_home_url() . '/afip/') );
            exit;
          }
        }
      } else {
        // No empleador
        $solo++;
      }

      $line = fgets( $handler );
    }

    check_dead_cuits();
    load_updates( $employeers, $solo);
    generate_afip_csv();

    fclose( $handler );
    unlink( $full_file );

    wp_redirect( add_query_arg( array( 'em' => $employeers,
                                       'sl' => $solo,
                                       'nu' => $not_found ),
                                get_home_url() . '/afip/' ) );
    exit;
  }
}

function procesa_cuits_afip() {
  if( empty( $_POST[ 'afipFile' ] ) ):
    wp_redirect( add_query_arg( array( 'errormsg' => "ERROR: No se encuentra el archivo" ),
                                get_home_url() . '/afip/') );
    exit;
  endif;

  global $wpdb;

  $path = '/var/www/html/wp-content/uploads/fop/afip/';
  $full_file = $path . 'utlfile/padr/' . $_POST[ 'afipFile' ];
  $handler = fopen( $full_file, "r" );

  if( $handler ) {
    $employeers = 0;
    $solo       = 0;
    $not_found  = 0;

    //prepare_afip_db();

    while( !feof( $handler ) ) {
      $line = fgets( $handler );

      if( $line[ 18 ] == 'S' ) {
        // Empleador
        $employeers++;

        $cuit = array( substr( $line, 0, 11 ) );
        if( insert_afipdb_active( $cuit[0] ) == false ) {
          //Empleador: AFIP retorna ERROR
          $not_found++;

          $wpdb->insert( 'fop_afip_errors', 
                         array( 'errorCUIT' => $cuit[0],
                                'errorDate' => current_time( 'mysql', 1 ) ) ); 
          if( $not_found > 2 ) {
            fclose( $handler );
            wp_redirect( add_query_arg( array( 'afip' => "Errores en el Servidor AFIP", 
                                               'f' => $_POST[ 'afipFile' ],
                                               'lc' => $cuit[0],
                                               'emp' => $employeers,
                                               'solo' => $solo,
                                               'nfound' => $not_found ),
                                        get_home_url() . '/afip/') );
            exit;
          }
        }
      } else {
        // No empleador
        $solo++;
      }
    }

    check_dead_cuits();
    load_updates( $employeers, $solo);
    generate_afip_csv();

    fclose( $handler );
    unlink( $full_file );

    wp_redirect( add_query_arg( array( 'em' => $employeers,
                                       'sl' => $solo,
                                       'nu' => $not_found ),
                                get_home_url() . '/afip/' ) );
    exit;
  }
}

function insert_afipdb_active( $cuit ) {
  if( cuit_exists( $cuit ) ) {
    return true;
  }

  global $wpdb;

  try {
    $afip   = new Afip( array( 'CUIT' => 20275093123, 'production' => TRUE ) );
    $person = $afip->RegisterScopeFive->GetTaxpayerDetails( $cuit );
  } catch (Exception $e) {
      return false;
  }

  if( is_null( $person ) ) {
    //CUIT NO ENCONTRADA
    return false;
  } else {
    //CUIT
    $cuit = $person->datosGenerales->idPersona;

    //Razon Social o Apellido, Nombre
    $razon_social = $person->datosGenerales->razonSocial;
    if( $razon_social == "" ) {
      $razon_social = $person->datosGenerales->apellido . ", " . $person->datosGenerales->nombre;
    }

    //Provincia
    $provincia = $person->datosGenerales->domicilioFiscal->descripcionProvincia;

    //Codigo Postal
    $codigo_postal = $person->datosGenerales->domicilioFiscal->codPostal;
 
    //Localidad
    $localidad = $person->datosGenerales->domicilioFiscal->localidad;

    //Direccion
    $direccion = $person->datosGenerales->domicilioFiscal->direccion;

    //ID Actividades
    if( count( $person->datosRegimenGeneral->actividad ) == 1 ) {
      $actividades = $person->datosRegimenGeneral->actividad->idActividad;
    } else {
      foreach( $person->datosRegimenGeneral->actividad as $actividad ) {
        $actividades .= $actividad->idActividad;
        $actividades .= "-";
      }
      $actividades = rtrim( $actividades, "-" );
    }

    $wpdb->insert( 'fop_afip_companies', 
                   array( 'afipCUIT' => $cuit,
                          'afipDenominacion' => $razon_social,
                          'afipActividades' => $actividades,
                          'afipProvincia' => $provincia,
                          'afipCodigoPostal' => $codigo_postal,
                          'afipLocalidad' => $localidad,
                          'afipDireccion' => $direccion,
                          'afipActivo' => 1,
                          'afipDesde' => current_time( 'mysql', 1 ) ) );
  }
  return true;
}

function cuit_exists( $cuit ) {
  global $wpdb;

  $exists_query = "SELECT afipId FROM fop_afip_companies WHERE afipCUIT = " . $cuit;
  $exists = $wpdb->get_var( $exists_query );
 
  if( $exists != NULL ) {
    $dead_query = "SELECT afipHasta FROM fop_afip_companies WHERE afipCUIT = " . $cuit;
    $dead_date = $wpdb->get_var( $dead_query );
    if( $dead_date != NULL ) {
      return false;
    } else {
      $update_query = "UPDATE fop_afip_companies SET afipActivo = 1 WHERE afipId = " . $exists;
      $wpdb->query( $update_query );

      return true;
    }
  }

  return false;  
}

function prepare_afip_db() {
  global $wpdb;

  $update_query = "UPDATE fop_afip_companies SET afipActivo = 0 WHERE 1 = 1";
  $wpdb->query( $update_query );

  return true;
}

function check_dead_cuits() {
  global $wpdb;

  $rows = $wpdb->get_results( "SELECT * FROM fop_afip_companies");

  foreach( $rows as $row ) { 
    if( $row->afipActivo == 0 ) {
      $update_query = "UPDATE fop_afip_companies SET afipHasta = '" . current_time( 'mysql', 1 )  . "' WHERE afipId = " . $row->afipId;
      $wpdb->query( $update_query );
    }
  }
}

function load_updates( $employeers, $solo ) {
  global $wpdb;

  $wpdb->insert( 'fop_afip_updates',
                 array( 'updateDate' => current_time( 'mysql', 1 ),
                        'cuitTotal' => ( $employeers + $solo ),
                        'cuitEmpleadores' => $employeers ) );
  return true;  
}

function extract_afip_zip_file( $afip_file ) {
  $path = '/var/www/html/wp-content/uploads/fop/afip/';
  $search = $path . 'utlfile/padr/*.tmp';

  $zip = new ZipArchive;

  if( $zip->open( $afip_file ) === TRUE) {
    $zip->extractTo( $path );
    $zip->close();

    $files = glob( $search );

    return basename( $files[ 0 ] );
  } else {
    return false;
  }
}


add_action( 'admin_post_uploadAfip', 'captura_activos_afip' );
add_action( 'admin_post_processAfip', 'procesa_cuits_afip' );
add_action( 'admin_post_reprocessAfip', 'reprocesa_cuits_afip' );

?>

