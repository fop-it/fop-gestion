# Gestión FOP
## Guía para replicar y montar el servidor


Esta guía pretende documentar los pasos a seguir para replicar la [Plataforma de Gestión FOP](https://gestion.observatoriopyme.org.ar/) en un ambiente local o en un Cloud VPS.

#### Las tecnologías elegidas para el desarrollo son:
- OS CentOS 7 64-bit
- PHP 7.4
- MariaDB 10.5
- Apache Web Server
- Wordpress

## Entornos
### Local:
- VirtualBox v6.1.16
	- Check Hyper-V: Off
	- Check BIOS Virtualization Enabled (Intel or SVM Mode)
- CentOS 7 Minimal Version 64 bits
	- RAM 2GB
	- Network:
		VirtualBox BridgedNetwork on Adapter1
		Set /etc/sysconfig/network-scripts/ifcfg-xxxx to ONBOOT=yes
	- SSH
	    ```sh 
        systemctl enable sshd
        vi /etc/ssh/sshd_config
	        Set PermitRootLogin no
	            Port 22
        reboot
        ```
### Remoto:
- Creación de usuario Admin
    ```sh
    sudo ssh root@1.1.1.1 -p2222
    sudo adduser UserName
    sudo passwd UserName 
    sudo usermod -aG wheel UserName
    ```

- Eliminar usuario
    ```sh
    sudo userdel -r UserName
    ```

## Set Up (común a ambos entornos)
- ### OS 
    ```sh
    sudo yum -y update
    sudo yum install -y yum-utils unzip wget firewalld
    sudo systemctl unmask firewalld
    sudo systemctl enable firewalld
    ```
- ### Apache
    ```sh
    sudo yum install -y httpd 
    sudo systemctl start httpd
    sudo systemctl enable httpd
    ```
- ### PHP 7.4 | PHP-FPM
    ```sh
    sudo yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
    sudo yum -y install https://rpms.remirepo.net/enterprise/remi-release-7.rpm
    sudo yum-config-manager --enable remi-php74
    sudo yum update
    sudo yum --enablerepo=remi-php74 install -y php php-common php-opcache php-mcrypt php-cli php-gd php-curl php-mysqlnd  php-mbstring  php-imagick php-bcmath php-xml php-pecl-zip php-soap
    sudo systemctl restart httpd
    ```
- ### MariaDB 10.5
    ```sh
    sudo tee /etc/yum.repos.d/mariadb.repo<<EOF
    [mariadb]
    name = MariaDB
    baseurl = http://yum.mariadb.org/10.5/centos7-amd64
    gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
    gpgcheck=1
    EOF

    sudo yum makecache
    sudo yum install -y MariaDB-server MariaDB-client
    sudo systemctl start mariadb
    sudo systemctl enable mariadb
    sudo firewall-cmd --add-service=mysql --permanent
    sudo firewall-cmd --reload
    ```

- Configurar seguridad de MariaDB
    ```sh
    sudo mysql_secure_installation
       -> Switch to unix_socket authentication [Y/n]  n
           Set root password? [Y/n] Y -> Observatorio.11-20
           Remove anonymous users? [Y/n] y
           Disallow root login remotely? [Y/n] y
           Remove test database and access to it? [Y/n] y
           Reload privilege tables now? [Y/n] y
    ```
- Crear la base de datos de Wordpress
    ```sh
    sudo mysql -u root -p
	    MariaDB [(none)]> CREATE DATABASE wpdb;
	    MariaDB [(none)]> GRANT ALL PRIVILEGES on wpdb.* to 'user'@'localhost' identified by 'password';
	    MariaDB [(none)]> FLUSH PRIVILEGES;
	    MariaDB [(none)]> EXIT;
    ```

- ### Wordpress
    ```sh
    cd /tmp
    wget http://wordpress.org/latest.tar.gz
    tar -xzvf latest.tar.gz
    sudo cp -avr wordpress/* /var/www/html/
    sudo mkdir /var/www/html/wp-content/uploads
    sudo chown -R apache:apache /var/www/html/
    sudo chmod -R 755 /var/www/html/

    cd /var/www/html/
    sudo mv wp-config-sample.php wp-config.php
    sudo vi wp-config.php
       -> define('DB_NAME', 'wpdb');
            define('DB_USER', 'user');
            define('DB_PASSWORD', 'password');

    sudo firewall-cmd --permanent --zone=public --add-service=http
    sudo firewall-cmd --permanent --zone=public --add-service=https
    sudo firewall-cmd --reload
    ```

- ## SOLO AMBIENTE LOCAL
    ```sh
    sudo vi /var/www/html/wp-config.php
        -> define( 'FS_METHOD','direct' );
        -> define( 'WP_HOME', 'http://XXX.XXX.XXX.XXX' );
        -> define( 'WP_SITEURL', 'http://XXX.XXX.XXX.XXX' );
    ```

- ### WP-CLI
    ```sh
    sudo curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
    php wp-cli.phar --info
    sudo chmod +x wp-cli.phar
    sudo mv wp-cli.phar /usr/bin/wp
    wp --info
    wp cli version
    sudo wp cli update
    ```

- ### PHPSPREADSHEET
    ```sh
    cd /var/www/html
    sudo php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    sudo php composer-setup.php
    sudo php -r "unlink('composer-setup.php');"

    sudo php composer.phar require phpoffice/phpspreadsheet
    sudo php composer.phar update
    ```

- ## Si existe problema al acceder a la página
    ```sh
    sudo firewall-cmd --permanent --zone=public --add-service=http
    sudo firewall-cmd --permanent --zone=public --add-service=https
    sudo firewall-cmd --reload
    ```

- ### i18n
    ```sh
    sudo yum install -y gettext

    Crear el archivo con terminación es_AR.po a partir del archivo .pot y luego correr el comando:
    
    msgfmt -o wp-content/plugins/{PLUGIN}/languages/{PLUGIN}-es_AR.mo wp-content/plugins/{PLUGIN}/languages/{PLUGIN}-es_AR.po
    ```

## En este punto, tenemos el entorno de desarrollo preparado para comenzar a trabajar.
- Acceder mediante el explorador de internet a la dirección IP asignada en el entorno local o al VPS, terminar la instalación de WordPress siguiendo los pasos.
- Una vez parados en el escritorio
  - Ingresar a Plugins -> New Plugin
  - Instalar All-in-One WP Migration + File Size upgrade
  - Activar Plugin
  - Importar el archivo wpress con el último backup disponible desde el Plugin
  - Sitio listo!



## Consideraciones
### Acceso al servidor
Se deshabilitó el ingreso por SSH para evitar ataques por fuerza bruta.
Seguir esta [guía](https://linuxize.com/post/how-to-set-up-ssh-keys-on-centos-7/) para configurar acceso con claves SSH.

### Certificado SSL

Seguir esta [guía](https://linuxize.com/post/secure-apache-with-let-s-encrypt-on-centos-7/)

Tener en cuenta que hay que modificar el archivo /etc/httpd/conf.d/gestion.observatoriopyme.org.ar y eliminar todo lo relacionado HTTP en el puerto 443 y el alias con www

### SMTP
Seguir esta [guía](https://kinsta.com/es/base-de-conocimiento/configurar-sendgrid-wordpress/)

