<?php

include_once 'fop-utils.php';


function geolocalizar_afip() {
  global $wpdb;

  limpiar_localidades();

  $empty_loc_query = "SELECT * FROM fop_afip_companies " .
                     "WHERE afipLatitud IS NULL OR afipLatitud = '' ".
                     "ORDER BY afipCUIT";

  $row = $wpdb->get_row( $empty_loc_query );

  while( $row != NULL ) {
    $localidad = $row->afipLocalidad;
    $codigo_postal = $row->afipCodigoPostal;
    $provincia = $row->afipProvincia;

    get_location_from_address( $localidad, $codigo_postal, $provincia );

    $row = $wpdb->get_row( $empty_loc_query );
  }

  generate_afip_csv();

  wp_redirect( get_home_url() . '/geo/' );
  exit;
}

function get_location_from_address( $localidad, $codigo_postal, $provincia ) {
  global $wpdb;

  $pais = "ARGENTINA";

  $file = '/var/www/html/wp-content/uploads/fop/geo.txt';
  $fp = fopen( $file, 'a' );

  if( $localidad == NULL || $localidad == '' ) {
    $caba_localidad = $wpdb->get_var( "SELECT cpBarrio FROM fop_cp_caba " .
                                      "WHERE cpCodigo = " . $codigo_postal );

    $update_query = 'UPDATE fop_afip_companies ' .
                    'SET afipLocalidad = "' . trim( strtoupper( $caba_localidad ) ) .
                    '" WHERE afipCodigoPostal = ' . $codigo_postal;
    $wpdb->query( $update_query );

    $address = urlencode( trim( $caba_localidad ) . "," . trim( $codigo_postal ) . "," . trim( $provincia ) . "," . $pais );
  } else {
    $address = urlencode( trim( $localidad ) . "," . trim( $codigo_postal ) . "," . trim( $provincia ) . "," . $pais );
  }

  $url  = "http://nominatim.openstreetmap.org/?format=json&addressdetails=1&q=";
  $url .= $address;
  $url .= "&format=json&limit=1&email=mmassollo@observatoriopyme.org.ar";

  fwrite( $fp, $url );
  fwrite( $fp, "\n" );

  $data = '';
  $opts = array( 'http' => array( 'header' => "Content-type: text/html\r\nContent-Length: " .
                                                  strlen( $data ) . "\r\n", 'method' => 'POST' ), );
  $context  = stream_context_create( $opts );
  $response = file_get_contents( $url, false, $context );
  $location  = json_decode( $response, true );

  if( $response === FALSE || empty( $location ) ) {
    $address = urlencode( trim( $codigo_postal ) . "," . trim( $provincia ) );

    $url  = "http://nominatim.openstreetmap.org/?format=json&addressdetails=1&q=";
    $url .= $address;
    $url .= "&format=json&limit=1&email=mmassollo@observatoriopyme.org.ar";

    $data = '';
    $opts = array( 'http' => array( 'header' => "Content-type: text/html\r\nContent-Length: " .
                                                  strlen( $data ) . "\r\n", 'method' => 'POST' ), );
    $context  = stream_context_create( $opts );
    $response = file_get_contents( $url, false, $context );
    $location  = json_decode( $response, true );
  }

  fwrite( $fp, $location[0]['lat'] . "|" . $location[0]['lon'] );
  fwrite( $fp, "\n" );

  insert_geolocation_indb( $localidad, $codigo_postal, $provincia, $location[0]['lat'], $location[0]['lon'] );

  return true;
}

function insert_geolocation_indb( $localidad, $codigo_postal, $provincia, $latitude, $longitude ) {
  global $wpdb;

  $file = '/var/www/html/wp-content/uploads/fop/geo.txt';
  $fp = fopen( $file, 'a' );

  if( $localidad == NULL ) {
    $update_query = 'UPDATE fop_afip_companies ' .
                    'SET afipLatitud = "' . $latitude .
                    '", afipLongitud = "' . $longitude .
                    '" WHERE afipProvincia = "' . $provincia . 
                    '" AND afipCodigoPostal = ' . $codigo_postal;
    $wpdb->query( $update_query );
  } else {
    $update_query = 'UPDATE fop_afip_companies ' .
                    'SET afipLatitud = "' . $latitude .
                    '", afipLongitud = "' . $longitude .
                    '" WHERE afipProvincia = "' . $provincia . 
                    '" AND afipLocalidad = "' . $localidad . 
                    '" AND afipCodigoPostal = ' . $codigo_postal;
    $wpdb->query( $update_query );
  }

  fwrite( $fp, $wpdb->last_query );
  fwrite( $fp, "\n" );
  fwrite( $fp, $wpdb->last_error );
  fwrite( $fp, "\n" );

  return true;
}

function limpiar_localidades() {
  global $wpdb;

  $rows = $wpdb->get_results( 'SELECT afipId, afipLocalidad ' .
                              'FROM fop_afip_companies ' .
                              'WHERE afipLocalidad LIKE "% *%"' );

  foreach( $rows as $row ) {
    $localidad = trim( str_replace( "*", "", $row->afipLocalidad ) );

    $update_query = 'UPDATE fop_afip_companies ' .
                    'SET afipLocalidad = "' . $localidad .
                    '" WHERE afipId = ' . $row->afipId;
    $wpdb->query( $update_query );
  }
}

function crear_mapa_afip() {
  global $wpdb;

  $prov_query = 'SELECT * FROM fop_provinces';
  $provinces = $wpdb->get_results( $prov_query );

  foreach( $provinces as $province ) {
    crear_mapas_afip( $province->provDescription );
  }

  crear_mapas_afip( 'FULL' );
  crear_mapas_afip( 'DEAD' );

  wp_redirect( get_home_url() . '/geo/' );
  exit;
}

function crear_mapas_afip( $provincia ) {
  global $wpdb;
  $path = '/var/www/html/wp-content/uploads/fop/afip/';

  if( strcmp( 'FULL', $provincia ) ) {
    $coord_query = 'SELECT afipCodigoPostal, afipLatitud, afipLongitud, afipLocalidad, count(*) as Count ' .  
                   'FROM fop_afip_companies ' . 
                   'WHERE afipLatitud IS NOT NULL ' . 
                   'AND afipProvincia = "' . $provincia . '" ' .
                   'AND afipActivo = 1 ' .
                   'GROUP BY afipLocalidad ' .
                   'ORDER BY Count';
  } else {
    $coord_query = 'SELECT afipCodigoPostal, afipLatitud, afipLongitud, afipLocalidad, count(*) as Count ' .
                   'FROM fop_afip_companies ' .
                   'WHERE afipLatitud IS NOT NULL ' .
                   'AND afipActivo = 1 ' .
                   'GROUP BY afipLocalidad ' .
                   'ORDER BY Count';
  }

  if( !strcmp( 'DEAD', $provincia ) ) {
    $coord_query = 'SELECT afipCodigoPostal, afipLatitud, afipLongitud, afipLocalidad, count(*) as Count ' .
                   'FROM fop_afip_companies ' .
                   'WHERE afipLatitud IS NOT NULL ' .
                   'AND afipActivo = 0 ' .
                   'GROUP BY afipLocalidad ' .
                   'ORDER BY Count';
  }

  $spots = $wpdb->get_results( $coord_query );

  $dom = new DOMDocument( '1.0', 'UTF-8' );

  $node = $dom->createElementNS( 'http://earth.google.com/kml/2.1', 'kml' );
  $parNode = $dom->appendChild( $node );
  $dnode = $dom->createElement( 'Document' );
  $docNode = $parNode->appendChild( $dnode );

  $upto_50_style_node = $dom->createElement( 'Style' );
  $upto_50_style_node->setAttribute( 'id', 'upto50Style' );
  $upto_50_icon_style_node = $dom->createElement( 'IconStyle' );
  $upto_50_icon_style_node->setAttribute( 'id', 'upto50Icon' );
  $upto_50_icon_node = $dom->createElement( 'Icon' );
  $upto_50_href = $dom->createElement( 'href', 'http://maps.google.com/mapfiles/kml/paddle/grn-blank.png');
  $upto_50_icon_node->appendChild( $upto_50_href );
  $upto_50_icon_style_node->appendChild( $upto_50_icon_node );
  $upto_50_style_node->appendChild( $upto_50_icon_style_node );
  $docNode->appendChild( $upto_50_style_node );

  $upto_500_style_node = $dom->createElement( 'Style' );
  $upto_500_style_node->setAttribute( 'id', 'upto500Style' );
  $upto_500_icon_style_node = $dom->createElement( 'IconStyle' );
  $upto_500_icon_style_node->setAttribute( 'id', 'upto500Icon' );
  $upto_500_icon_node = $dom->createElement( 'Icon' );
  $upto_500_href = $dom->createElement( 'href', 'http://maps.google.com/mapfiles/kml/paddle/ylw-blank.png');
  $upto_500_icon_node->appendChild( $upto_500_href );
  $upto_500_icon_style_node->appendChild( $upto_500_icon_node );
  $upto_500_style_node->appendChild( $upto_500_icon_style_node );
  $docNode->appendChild( $upto_500_style_node );

  $upto_2000_style_node = $dom->createElement( 'Style' );
  $upto_2000_style_node->setAttribute( 'id', 'upto2000Style' );
  $upto_2000_icon_style_node = $dom->createElement( 'IconStyle' );
  $upto_2000_icon_style_node->setAttribute( 'id', 'upto2000Icon' );
  $upto_2000_icon_node = $dom->createElement( 'Icon' );
  $upto_2000_href = $dom->createElement( 'href', 'http://maps.google.com/mapfiles/kml/paddle/pink-blank.png');
  $upto_2000_icon_node->appendChild( $upto_2000_href );
  $upto_2000_icon_style_node->appendChild( $upto_2000_icon_node );
  $upto_2000_style_node->appendChild( $upto_2000_icon_style_node );
  $docNode->appendChild( $upto_2000_style_node );

  $pass_2000_style_node = $dom->createElement( 'Style' );
  $pass_2000_style_node->setAttribute( 'id', 'pass2000Style' );
  $pass_2000_icon_style_node = $dom->createElement( 'IconStyle' );
  $pass_2000_icon_style_node->setAttribute( 'id', 'pass2000Icon' );
  $pass_2000_icon_node = $dom->createElement( 'Icon' );
  $pass_2000_href = $dom->createElement( 'href', 'http://maps.google.com/mapfiles/kml/paddle/purple-blank.png');
  $pass_2000_icon_node->appendChild( $pass_2000_href );
  $pass_2000_icon_style_node->appendChild( $pass_2000_icon_node );
  $pass_2000_style_node->appendChild( $pass_2000_icon_style_node );
  $docNode->appendChild( $pass_2000_style_node );

  foreach( $spots as $spot ) {
    $node = $dom->createElement( 'Placemark' );
    $placeNode = $docNode->appendChild( $node );

    $pin_desc = "CP: " . $spot->afipCodigoPostal;
    $placeNode->setAttribute( 'id', $spot->afipCodigoPostal );

    $nameNode = $dom->createElement( 'name', htmlentities( $pin_desc ) );
    $placeNode->appendChild( $nameNode );
    $descNode = $dom->createElement( 'description', $spot->afipLocalidad . ": " . $spot->Count . " empresas." );
    $placeNode->appendChild( $descNode );

    if( $spot->Count <= 50 ) { $style = '#upto50Style'; }
    elseif( $spot->Count <= 500 ) { $style = '#upto500Style'; }
    elseif( $spot->Count <= 2000 ) { $style = '#upto2000Style'; }
    elseif( $spot->Count > 2000 ) { $style = '#pass2000Style'; }

    $styleUrl = $dom->createElement( 'styleUrl', $style );
    $placeNode->appendChild( $styleUrl );

    $pointNode = $dom->createElement( 'Point' );
    $placeNode->appendChild( $pointNode );

    $coorStr = $spot->afipLongitud . ','  . $spot->afipLatitud;
    $coorNode = $dom->createElement( 'coordinates', $coorStr );
    $pointNode->appendChild( $coorNode );
  }

  $dom->save( $path . 'afipMap' . str_replace( ' ', '', $provincia ) . '.kml' );

  return true;
}


add_action( 'admin_post_geoAfip', 'geolocalizar_afip' );
add_action( 'admin_post_geoMapa', 'crear_mapa_afip' );

?>

