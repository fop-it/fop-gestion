<?php

include_once 'fop-utils.php';


function captura_verificar_usuario() {
  if( empty( $_POST[ 'txtEmail' ] ) || !filter_var( $_POST[ 'txtEmail' ], FILTER_VALIDATE_EMAIL ) ):
    wp_redirect( add_query_arg( array( 'errormsg' => "ERROR: Email inválido. Reingrese el dato." ), 
                                       get_home_url() . '/eliminar-usuario') );
    exit;
  endif;

  $email = sanitize_text_field( $_POST[ 'txtEmail' ] );

  $user_db_data = getUserData( $email );

  if( $user_db_data ) {
    $name  = $user_db_data->user_nicename;
    $email = $user_db_data->user_email;
    $id    = get_user_by('email', $email )->id;
    $role  = get_userdata($id)->roles[0];
    $date  = $user_db_data->user_registered;
 
    if( strcmp( $role, "encuestador" ) && strcmp( $role, "marketing" ) && strcmp( $role, "reporte" ) ) {
      wp_redirect( add_query_arg( array( 'errormsg' => "ERROR: Sólo podrá eliminar usuarios de roles: Encuestador/Comunicaciones/Reportes" ),
                                         get_home_url() . '/eliminar-usuario/') );

      exit;
    }

    wp_redirect( add_query_arg( array( 'd_id' => $id,
                                       'd_name' => $name,
                                       'd_email' => $email,
                                       'd_role' => $role,
                                       'd_date' => $date ),
                                       get_home_url() . '/eliminar-usuario') );
    exit;
  } else {
    wp_redirect( add_query_arg( array( 'errormsg' => "ERROR: El usuario que desea eliminar no existe." ), 
                                       get_home_url() . '/eliminar-usuario') );
    exit;
  }
}

function captura_eliminar_usuario() {
  $user_id    = sanitize_text_field( $_POST[ 'txtId' ] );
  $user_email = sanitize_text_field( $_POST[ 'txtEmail' ] );
  $user_role  = sanitize_text_field( $_POST[ 'txtRole' ] );
  $user_date  = sanitize_text_field( $_POST[ 'txtDate' ] );

  if ( !function_exists( 'wp_delete_user' ) ) { 
    require_once( ABSPATH.'wp-admin/includes/user.php' );
  } 

  wp_delete_user( $user_id );

  setDeletedUserInDb( $user_id, $user_email, $user_role, $user_date );

  getUsersDbAsCSV();
  getDeletedUsersDbAsCSV();

  wp_redirect( get_home_url() . '/usuarios' );
  exit;
}


add_action( 'admin_post_verifyUser', 'captura_verificar_usuario' );
add_action( 'admin_post_deleteUser', 'captura_eliminar_usuario' );

?>

