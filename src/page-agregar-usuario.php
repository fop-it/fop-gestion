<?php get_header(); ?>

<div id="page-content" class="page-content">
  <div>
    <div style="height:140px" aria-hidden="true" class="wp-block-spacer">
    </div>

    <div class="fop-form">
      <?php if( isset ( $_GET[ 'errormsg' ] ) ): ?>
        <div>
          <p style="color:red; font-weight:bold;"> <?php echo $_GET[ 'errormsg' ]; ?>  </p>
        </div>
      <?php endif; ?>
      <h5> Complete los campos y presione Agregar </h5>

      <br>
      <form action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post">
        <label for="txtNombre">Nombre: </label>
        <input type="text" id="txtNombre" name="txtNombre" style="width: 400px;">
 
        <br>
        <label for="txtApellido">Apellido: </label>
        <input type="text" id="txtApellido" name="txtApellido" style="width: 400px;">

        <br>
        <label for="txtEmail">Email:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label>
        <input type="text" id="txtEmail" name="txtEmail" style="width: 400px;">

        <div align="left">
          <p>
            &nbsp;&nbsp;&nbsp;&nbsp;
            Rol:
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

            <input type="radio" id="pollster" name="radioRole" value="encuestador" checked>
            <label for="pollster">Encuestador</label> &nbsp;&nbsp;&nbsp;&nbsp;
        
            <input type="radio" id="marketing" name="radioRole" value="marketing">
            <label for="marketing">Comunicaciones</label>
         
            <input type="radio" id="reporter" name="radioRole" value="reporte">
            <label for="marketing">Reportes</label>
          </p>
        </div>

        <br>
        <input type="submit" class="fop-button" value="Agregar Usuario" >

        <input type="hidden" name="action" value="addUser">
      </form>
    </div>

    <div style="text-align: center; ">
      <br> <br>
      <a href="/usuarios">
       <input type="submit" class="fop-button" value="Volver a Usuarios">
      </a>
      <br> 
    </div>

  </div>
</div>

<?php get_footer(); ?>

