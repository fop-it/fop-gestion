<?php get_header(); ?>

<div id="page-content" class="page-content">
  <div>
    <div style="height:140px" aria-hidden="true" class="wp-block-spacer"></div>

    <table class="blueTable" style="width:400px" class="center">
      <thead>
        <tr>
          <th>Panel</th>
          <th>Creador</th>
          <th>Fecha</th>
        </tr>
      </thead>
      <tbody>
      <?php
        global $wpdb;
        $result = $wpdb->get_results( "SELECT * FROM fop_panels" );

        foreach( $result as $row ) : ?>
          <tr>
            <td><?php echo $row->panelName; ?></td>
            <td><?php echo $row->panelCreator; ?></td>
            <td><?php echo $row->panelDate; ?></td>
          </tr>
     <?php endforeach;?>
      </tbody>
    </table>

    <div style="text-align: center; ">
      <br> <br>
      <a href="/universo-de-empresas/">
       <input type="submit" class="fop-button" value="Volver a Empresas">
      </a>
      <br>
    </div>

  </div>
</div>

<?php get_footer(); ?>

