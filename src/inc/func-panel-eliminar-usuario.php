<?php

include_once 'fop-utils.php';


function captura_verificar_usuario_panel() {
  if( empty( $_POST[ 'txtEmail' ] ) || !filter_var( $_POST[ 'txtEmail' ], FILTER_VALIDATE_EMAIL ) ):
    wp_redirect( add_query_arg( array( 'errormsg' => "ERROR: Email inválido. Reingrese el dato." ), 
                                       get_home_url() . '/panel-eliminar-usuario') );
    exit;
  endif;

  $email = sanitize_text_field( $_POST[ 'txtEmail' ] );

  $user_db_data = getUserData( $email );

  if( $user_db_data ) {
    $name  = $user_db_data->user_nicename;
    $email = $user_db_data->user_email;
    $id    = get_user_by('email', $email )->id;
    $role  = get_userdata($id)->roles[0];
    $date  = $user_db_data->user_registered;

    wp_redirect( add_query_arg( array( 'd_id' => $id,
                                       'd_name' => $name,
                                       'd_email' => $email,
                                       'd_role' => $role,
                                       'd_date' => $date ),
                                       get_home_url() . '/panel-eliminar-usuario') );
    exit;
  } else {
    wp_redirect( add_query_arg( array( 'errormsg' => "ERROR: El usuario que desea eliminar no existe." ), 
                                       get_home_url() . '/panel-eliminar-usuario') );
    exit;
  }
}

function captura_eliminar_usuario_panel() {
  $user_id    = sanitize_text_field( $_POST[ 'txtId' ] );
  $user_email = sanitize_text_field( $_POST[ 'txtEmail' ] );
  $user_role  = sanitize_text_field( $_POST[ 'txtRole' ] );
  $user_date  = sanitize_text_field( $_POST[ 'txtDate' ] );

  if ( !function_exists( 'wp_delete_user' ) ) { 
    require_once( ABSPATH.'wp-admin/includes/user.php' );
  } 

  wp_delete_user( $user_id );

  setDeletedUserInDb( $user_id, $user_email, $user_role, $user_date );

  getUsersDbAsCSV();
  getDeletedUsersDbAsCSV();

  wp_redirect( get_home_url() . '/panel-de-control' );
  exit;
}


add_action( 'admin_post_verifyUserPanel', 'captura_verificar_usuario_panel' );
add_action( 'admin_post_deleteUserPanel', 'captura_eliminar_usuario_panel' );

?>

