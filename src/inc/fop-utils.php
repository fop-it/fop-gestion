<?php

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader;

require_once '/var/www/html/vendor/autoload.php';


/*
* function:    getUsersDbAsCSV
* Description: Puts all Users DB register in a CSV file to feed users table and to download.
* Parameters:  NONE
* Returns:     NONE
*/
function getUsersDbAsCSV( ) {
  global $wpdb;
  $users = $wpdb->get_results ( "SELECT * FROM  wp_users" );

  $filename = "/var/www/html/wp-content/uploads/fop/usersDB.csv";

  $header_row = array(
    'ID',
    'Nombre de Usuario',
    'Email',
    'Fecha de Registro',
    'Rol'
  );

  $data_rows = array();

  foreach ($users as $user) {
    $utcTimezone = new DateTimeZone( 'UTC' );
    $argTimezone = new DateTimeZone( 'America/Argentina/Buenos_Aires' );
    $registered = new DateTime( $user->user_registered, $utcTimezone );
    $registered->setTimeZone( $argTimezone );

    $row = array(
        $user->ID,
        $user->user_nicename,
        $user->user_email,
        $registered->format( 'd/m/Y H:i:s' ),
        get_userdata( $user->ID )->roles[0]
    );
    $data_rows[] = $row;
  }

  $fp = fopen( $filename, 'w+');

  ob_clean();

  fputcsv( $fp, $header_row ); 
  foreach ( $data_rows as $data_row ) { 
    fputcsv( $fp, $data_row ); 
  } 

  fclose( $fp ); 
}

/*
* function:    getDeletedUsersDbAsCSV
* Description: Puts all Deleted Users DB register in a CSV file to feed users table and to download.
* Parameters:  NONE
* Returns:     NONE
*/
function getDeletedUsersDbAsCSV( ) {
  global $wpdb;
  $users = $wpdb->get_results ( "SELECT * FROM fop_deletedusers" );

  $filename = "/var/www/html/wp-content/uploads/fop/deletedUsersDB.csv";

  $header_row = array(
      'ID',
      'Email',
      'Rol',
      'Eliminado por',
      'Registrado',
      'Eliminado'
  );

  $data_rows = array();

  foreach ($users as $user) {
      $utcTimezone = new DateTimeZone( 'UTC' );
      $argTimezone = new DateTimeZone( 'America/Argentina/Buenos_Aires' );
      $registered  = new DateTime( $user->user_registered, $utcTimezone );
      $registered->setTimeZone( $argTimezone );
      $deleted    = new DateTime( $user->user_deleted );
      $row = array(
          $user->user_id,
          $user->user_email,
          $user->user_role,
          $user->deleted_by,
          $registered->format( 'd/m/Y H:i:s' ),
          $deleted->format( 'd/m/Y H:i:s' )
      );
      $data_rows[] = $row;
  }

  $fp = fopen( $filename, 'w+');

  ob_clean();

  fputcsv( $fp, $header_row ); 
  foreach ( $data_rows as $data_row ) { 
      fputcsv( $fp, $data_row ); 
  } 

  fclose( $fp ); 
}

/*
* function:    setDeletedUserInDb
* Description: Inserts deleted user in fop_deletedusers table in wp_db
* Parameters:  ID, Email, Role, Date
* Returns:     None
*/
function setDeletedUserInDb( $user_id, $user_email, $user_role, $user_date ) {
  global $wpdb;
  global $current_user;
  date_default_timezone_set('America/Argentina/Buenos_Aires');

  wp_get_current_user();

  $deleted_by  = $current_user->user_email;
  $actual_date = current_time( 'mysql' );

  $wpdb->insert("fop_deletedusers", array( "user_id" => $user_id,
                                           "user_email" => $user_email,
                                           "user_role" => $user_role,
                                           "deleted_by" => $deleted_by,
                                           "user_registered" => $user_date,
                                            "user_deleted" => $actual_date ) ); 
}

/*
* function:    getUserData
* Description: Returns Users DB record from email.
* Parameters:  Email
* Returns:     Record
*/
function getUserData( $user_email ) {
  global $wpdb;
  $query = $wpdb->prepare( "SELECT * FROM wp_users WHERE user_email = %s", $user_email );
  $result = $wpdb->get_row( $query );

  return $result;
}

/*
* function:    getUserID
* Description: Returns UserID from email.
* Parameters:  Email
* Returns:     User ID
*/
function getUserID( $user_email ) {
  global $wpdb;
  $query = $wpdb->prepare( "SELECT ID FROM wp_users WHERE user_email = %s", $user_email );
  $result = $wpdb->get_var( $query );

  return $result;
}

/*
* function:    checkUserExists
* Description: Checks if an email already exists in Users DB.
* Parameters:  Email to check
* Returns:     True if exists, False otherwise
*/
function checkUserExists( $user_email ) {
  global $wpdb;
  $query = $wpdb->prepare( "SELECT * FROM wp_users WHERE user_email = %s", $user_email );
  $result = $wpdb->get_results( $query );

  if( empty($result) ) {
    return false;
  }
  
  return true;
}

/*
* function:    getUserActivity
* Description: Returns User Activity.
* Parameters:  User ID, Date From, Date To
* Returns:     DB Records
*/
function getUserActivity( $user_id, $date_from, $date_to ) {
  global $wpdb;
  $query = $wpdb->prepare( "SELECT ua_action, ua_time " .
                           "FROM fop_user_activity " .
                           "WHERE user_id = %s " .
                           "AND ua_time BETWEEN %d AND %d " .
                           "ORDER BY ua_id ASC", $user_id, $date_from, strtotime('+1 day', $date_to) );
  $result = $wpdb->get_results( $query );

  return $result;
}

/*
* function:    downloadUserActivity
* Description: Downloads User Activity file.
* Parameters:  User ID, Date From, Date To, Email
* Returns:     None
*/
function downloadUserActivity( $user_id, $date_from, $date_to , $email ) {
  $mail_array = explode( "@", $email );
  $filename = 'actividad-' . $mail_array[ 0 ] . '.xlsx';
  $downloadFile = '/var/www/html/wp-content/uploads/fop/activity.csv';
  $output = fopen( $downloadFile, 'w+' );

  ob_clean();

  fputcsv( $output, array( $email ) );
  fputcsv( $output, array( 'Fecha', 'Conexión', 'Desconexión' ) );

  $results = getUserActivity( $user_id, $date_from, $date_to );

  $len = count( $results );
  $loop = 0;

  while ( $loop < $len ) {
    if( $results[ $loop ]->ua_action == "logged_in" ) {
      $begin   = new DateTime();
      $begin->setTimestamp( $results[ $loop ]->ua_time );
      $begin_m = $begin->format('m');
      $begin_d = $begin->format('d');

      if( $loop+1 == $len ) {
        $data = array( date( 'd/m/Y', $results[ $loop ]->ua_time ),
                       date( 'H:i:s', $results[ $loop ]->ua_time ),
                       '-' );
        fputcsv( $output, $data );
        $loop += 1;
      } else {
        $end   = new DateTime();
        $end->setTimestamp( $results[ $loop+1 ]->ua_time );
        $end_m = $end->format('m');
        $end_d = $end->format('d');

        if( $results[ $loop+1 ]->ua_action == "logged_out" ) {
          if( $end_m == $begin_m ) {
            if( $end_d == $begin_d ) {
              $data = array( date( 'd/m/Y', $results[ $loop ]->ua_time ),
                             date( 'H:i:s', $results[ $loop ]->ua_time ),
                             date( 'H:i:s', $results[ $loop+1 ]->ua_time ) );
              fputcsv( $output, $data );
              $loop += 2;
            } else {
                $data = array( date( 'd/m/Y', $results[ $loop ]->ua_time ),
                               date( 'H:i:s', $results[ $loop ]->ua_time ),
                               '-' );
                fputcsv( $output, $data );
                $data = array( date( 'd/m/Y', $results[ $loop+1 ]->ua_time ),
                               '-',
                               date( 'H:i:s', $results[ $loop+1 ]->ua_time ) );
                fputcsv( $output, $data );
                $loop += 2;
              }
          } else {
            $data = array( date( 'd/m/Y', $results[ $loop ]->ua_time ),
                           date( 'H:i:s', $results[ $loop ]->ua_time ),
                           '-' );
            fputcsv( $output, $data );
            $data = array( date( 'd/m/Y', $results[ $loop+1 ]->ua_time ),
                           '-',
                           date( 'H:i:s', $results[ $loop+1 ]->ua_time ) );
            fputcsv( $output, $data );
            $loop += 2;
          }      
        } else {
          $data = array( date( 'd/m/Y', $results[ $loop ]->ua_time ),
                         date( 'H:i:s', $results[ $loop ]->ua_time ),
                         '-' );
          fputcsv( $output, $data );
          $data = array( date( 'd/m/Y', $results[ $loop+1 ]->ua_time ),
                         date( 'H:i:s', $results[ $loop+1 ]->ua_time ), 
                         '-' );
          fputcsv( $output, $data );
          $loop += 2;
        }
      }
    } else {
      $data = array( date( 'd/m/Y', $results[ $loop ]->ua_time ), '-', date( 'H:i:s', $results[ $loop ]->ua_time ) );
      $loop += 1;

      fputcsv( $output, $data );
    }
  }

  downloadExcelFile( $downloadFile, $filename );

  unlink( $downloadFile );

  exit;
}

/*
* function:    downloadCsvFile
* Description: Receives CSV file and downloads it.
* Parameters:  Download file absolut path, File name of downloaded file
* Returns:     None
*/
function downloadCsvFile( $downloadFile, $filename ) {
  $path = "/var/www/html/wp-content/uploads/fop/";
  $zip_file = $path . $filename . ".zip";

  $zip = new ZipArchive();
  if( $zip->open( $zip_file, ZipArchive::CREATE) !== TRUE) {
    return false;
  }

  if( file_exists( $downloadFile ) ) {
    $zip->addFile( $downloadFile, $filename . ".csv" );
    $zip->close();

    header( 'Content-type: application/zip' );
    header( 'Content-Disposition: attachment; filename="' . basename( $zip_file ) . '"' );
    header( 'Content-length: ' . filesize( $zip_file ) );
    header( 'Pragma: no-cache' );
    header( 'Expires: 0' );

    ob_clean();
    flush();

    readfile( $zip_file );

    unlink( $zip_file );
    exit;
  }
}

/*
* function:    downloadExcelFile
* Description: Receives CSV file, transforms it to XLSX and downloads it.
* Parameters:  Download file absolut path, File name of downloaded file
* Returns:     None
*/
function downloadExcelFile( $downloadFile, $filename ) {
  if( file_exists( $downloadFile ) ) {
    $spreadsheet = new Spreadsheet();

    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
    $reader->setDelimiter( ',' );
    $reader->setEnclosure( '"' );
    $reader->setSheetIndex( 0 );

    $spreadsheet = $reader->load( $downloadFile );

    $writer = new Xlsx( $spreadsheet );
    header( 'Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' );
    header( 'Content-Disposition: attachment; filename="'. urlencode( $filename ).'"' );
    ob_end_clean();
    $writer->save( 'php://output' );

    $spreadsheet->disconnectWorksheets();
    unset( $spreadsheet );
  }
}

/*
* function:    downloadCompressedExcelFile
* Description: Receives CSV file, transforms it to XLSX, compresses and downloads it.
* Parameters:  Download file absolut path, File name of downloaded file
* Returns:     None
*/
function downloadCompressedExcelFile( $downloadFile, $filename ) {
  $path = "/var/www/html/wp-content/uploads/fop/";
  $temp_file = $path . $filename . ".xlsx";
  $zip_file = $path . $filename . ".zip";

  $zip = new ZipArchive();
  if( $zip->open( $zip_file, ZipArchive::CREATE) !== TRUE) {
    return false;
  }

  if( file_exists( $downloadFile ) ) {
    $spreadsheet = new Spreadsheet();

    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
    $reader->setDelimiter( ',' );
    $reader->setEnclosure( '"' );
    $reader->setSheetIndex( 0 );

    $spreadsheet = $reader->load( $downloadFile );

    $writer = new Xlsx( $spreadsheet );
    header( 'Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' );
    header( 'Content-Disposition: attachment; filename="'. urlencode( $temp_file ) . '"' );
    ob_end_clean();
    $writer->save( $temp_file );

    $zip->addFile( $temp_file, $filename . ".xlsx" );
    $zip->close();

    header( 'Content-type: application/zip' );
    header( 'Content-Disposition: attachment; filename="' . basename( $zip_file ) . '"' );
    header( 'Content-length: ' . filesize( $zip_file ) );
    header( 'Pragma: no-cache' );
    header( 'Expires: 0' );

    ob_clean();
    flush();

    readfile( $zip_file );

    $spreadsheet->disconnectWorksheets();
    unset( $spreadsheet );

    unlink( $zip_file );
    unlink( $temp_file );
    exit;
  }
}

/*
* function:    getUserEmailByRole
* Description: Returns Users ID and Email according to roles.
* Parameters:  Roles array.
* Returns:     Users emails array.
*/
function getUserEmailByRole( $roles ) {
  global $wpdb;

  if( ! is_array( $roles ) ) {
    $roles = array_walk( explode( ",", $roles ), 'trim' );
  }

  $sql = 'SELECT user_email
          FROM ' . $wpdb->users . ' INNER JOIN ' . $wpdb->usermeta . '
          ON ' . $wpdb->users . '.ID = ' . $wpdb->usermeta . '.user_id
          WHERE ' . $wpdb->usermeta . '.meta_key = \'' . $wpdb->prefix . 'capabilities\'
          AND ( ';

  $i = 1;

  foreach ( $roles as $role ) {
    $sql .= ' ' . $wpdb->usermeta . '.meta_value LIKE \'%"' . $role . '"%\' ';
    if ( $i < count( $roles ) ) $sql .= ' OR ';
    $i++;
  }

  $sql .= ' ) ';
  $sql .= ' ORDER BY ID ';

  $users_emails = $wpdb->get_col( $sql );

  return $users_emails;
}

/*
* function:    checkPanelFileHeader
* Description: Checks if Panel Excel file Header contains correct rows to parse.
* Parameters:  Panel excel file
* Returns:     True if OK, otherwise False
*/
function checkPanelFileHeader( $header ) {

  $base_header = '/var/www/html/wp-content/uploads/fop/panelHeaderRow.xlsx';

  $base_file_type   = \PhpOffice\PhpSpreadsheet\IOFactory::identify( $base_header );
  $base_reader      = \PhpOffice\PhpSpreadsheet\IOFactory::createReader( $base_file_type );
  $base_spreadsheet = $base_reader->load( $base_header );
  $base_content     = $base_spreadsheet->getActiveSheet()->toArray();

  $header_file_type   = \PhpOffice\PhpSpreadsheet\IOFactory::identify( $header );
  $header_reader      = \PhpOffice\PhpSpreadsheet\IOFactory::createReader( $header_file_type );
  $header_spreadsheet = $header_reader->load( $header );
  $header_content     = $header_spreadsheet->getActiveSheet()->toArray();

  $equals = array_udiff( $base_content[ 0 ], $header_content[ 0 ], 'strcasecmp' );
  if( empty( $equals ) ) {
    return true;
  }

  return false;
}

/*
* function:    checkPanelQuotaHeader
* Description: Checks if Panel Quota Excel file Header contains correct rows to parse.
* Parameters:  Panel quota excel file
* Returns:     True if OK, otherwise False
*/
function checkPanelQuotaHeader( $header ) {

  $base_header = '/var/www/html/wp-content/uploads/fop/panelQuota.xlsx';

  $base_file_type   = \PhpOffice\PhpSpreadsheet\IOFactory::identify( $base_header );
  $base_reader      = \PhpOffice\PhpSpreadsheet\IOFactory::createReader( $base_file_type );
  $base_spreadsheet = $base_reader->load( $base_header );
  $base_content     = $base_spreadsheet->getActiveSheet()->toArray();

  $header_file_type   = \PhpOffice\PhpSpreadsheet\IOFactory::identify( $header );
  $header_reader      = \PhpOffice\PhpSpreadsheet\IOFactory::createReader( $header_file_type );
  $header_spreadsheet = $header_reader->load( $header );
  $header_content     = $header_spreadsheet->getActiveSheet()->toArray();

  $equals = array_udiff( $base_content[ 0 ], $header_content[ 0 ], 'strcasecmp' );
  if( empty( $equals ) ) {
    return true;
  }

  return false;
}

/*
* function:    checkReassignPanelHeader
* Description: Checks if Reassign Panel Excel file Header contains correct rows to parse.
* Parameters:  Reassign panel excel file
* Returns:     True if OK, otherwise False
*/
function checkReassignPanelHeader( $header ) {

  $base_header = '/var/www/html/wp-content/uploads/fop/panelReassign.xlsx';

  $base_file_type   = \PhpOffice\PhpSpreadsheet\IOFactory::identify( $base_header );
  $base_reader      = \PhpOffice\PhpSpreadsheet\IOFactory::createReader( $base_file_type );
  $base_spreadsheet = $base_reader->load( $base_header );
  $base_content     = $base_spreadsheet->getActiveSheet()->toArray();

  $header_file_type   = \PhpOffice\PhpSpreadsheet\IOFactory::identify( $header );
  $header_reader      = \PhpOffice\PhpSpreadsheet\IOFactory::createReader( $header_file_type );
  $header_spreadsheet = $header_reader->load( $header );
  $header_content     = $header_spreadsheet->getActiveSheet()->toArray();

  $equals = array_udiff( $base_content[ 0 ], $header_content[ 0 ], 'strcasecmp' );
  if( empty( $equals ) ) {
    return true;
  }

  return false;
}

/*
* function:    loadPanelDB
* Description: Loads Panel fields in DB.
* Parameters:  Panel excel file, Panel Name.
* Returns:     True if OK, otherwise False
*/
function loadPanelDB( $uploaded_panel, $panel_name ) {
  global $wpdb;

  if( panelExists( $panel_name ) != -1 ) {
    return false;
  }

  date_default_timezone_set( 'America/Argentina/Buenos_Aires' );

  $current_user = wp_get_current_user();
  $current_email = $current_user->user_email;
  $current_email = $current_user->user_email;

  $panels_query = array( 'panelName' => $panel_name, 'panelCreator' => $current_email, 'panelDate' => current_time( 'mysql' ) );
  $wpdb->insert( "fop_panels", $panels_query );

  $file_type   = \PhpOffice\PhpSpreadsheet\IOFactory::identify( $uploaded_panel );
  $reader      = \PhpOffice\PhpSpreadsheet\IOFactory::createReader( $file_type );
  $spreadsheet = $reader->load( $uploaded_panel );
  $content     = $spreadsheet->getActiveSheet()->toArray();

  for( $loop = 1; $loop < count( $content ); $loop++ ) {
    $columns = $content[ $loop ];

    $companyId = companyExists( $columns[ 4 ] );

    if( $companyId == -1 ) {
      // New Company
      $company_contact = array( "contact" => array( 'telephone' => $columns[ 16 ] , 'last_contact' => $columns[ 15 ], 
                                                    'email_1' => $columns[ 17 ], 'email_2' => $columns[ 18 ], 'email_3' => $columns[ 19 ],
                                                    'email_4' => $columns[ 20 ], 'email_5' => $columns[ 21 ], 'email_6' => $columns[ 22 ],
                                                    'web' => $columns[ 14 ] ) );

      $company_panel = array( 'panel' => $panel_name, 'survey' => $columns[ 0 ] );

      $company_marketing = array( 'scoring' => $columns[ 2 ] );

      $company_action_log = array( array( 'time' => current_time( 'mysql' ), 
                                          'by' => $current_email,
                                          'action' => "Creacion de empresa por Panel" ) );

      $company_query = array( "companyCUIT" => $columns[ 4 ],
                              "companyVenue" => 0,
                              "companyName" => $columns[ 5 ],
                              "companyStatus" => 1,
                              "companyFopStratum" => $columns[ 8 ],
                              "companyCIIU" => $columns[ 7 ],
                              "companyBranch" => $columns[ 3 ],
                              "companyEmployees" => $columns[ 6 ],
                              "companyRegion" => $columns[ 9 ],
                              "companySurvey" => json_encode( $company_panel ),
                              "companyMarketing" => json_encode( $company_marketing ),
                              "companyActionLog" => json_encode( $company_action_log ) );

      $wpdb->insert( "fop_panels_companies", $company_query );

      $company_id = $wpdb->get_var( "SELECT CompanyId FROM fop_panels_companies WHERE companyCUIT ='" . $columns[ 4 ] . "'" );
 
      $contact_query = array( "companyId" => $company_id,
                              "companyStreet" => $columns[ 13 ],
                              "companyProvince" => $columns[ 10 ],
                              "companyLocation" => $columns[ 11 ],
                              "companyZone" => $columns[ 12 ],
                              "companyStNumber" => 0, 
                              "companyFloor" => 0, 
                              "companyOffice" => 0, 
                              "companyPostCode" => 0, 
                              "companyContact" => json_encode( $company_contact ) );

      $wpdb->insert( "fop_panels_companies_contact", $contact_query );
   } else {
      // Company exists
    }
  }
  
  return true;
}

/*
* function:    loadQuotaDB
* Description: Loads quota fields in DB.
* Parameters:  Quota panel excel file, Panel name.
* Returns:     True if OK, otherwise False
*/
function loadQuotaDB( $uploaded_quota, $panel_name ) {
  global $wpdb;

  date_default_timezone_set('America/Argentina/Buenos_Aires');

  $current_user = wp_get_current_user();
  $current_email = $current_user->user_email;

  $panel_id = $wpdb->get_var("SELECT panelId FROM fop_panels WHERE panelName = '" . $panel_name . "'" );

  $file_type   = \PhpOffice\PhpSpreadsheet\IOFactory::identify( $uploaded_quota );
  $reader      = \PhpOffice\PhpSpreadsheet\IOFactory::createReader( $file_type );
  $spreadsheet = $reader->load( $uploaded_quota );
  $content     = $spreadsheet->getActiveSheet()->toArray();

  for( $loop = 1; $loop < count( $content ); $loop++ ) {
    $columns = $content[ $loop ];

    $companyId = companyExists( $columns[ 4 ] );

    if( $companyId == -1 ) {
      // New Company
      $company_contact = array( "contact" => array( 'telephone' => $columns[ 16 ] , 'last_contact' => $columns[ 15 ], 
                                                    'email_1' => $columns[ 17 ], 'email_2' => $columns[ 18 ], 'email_3' => $columns[ 19 ],
                                                    'email_4' => $columns[ 20 ], 'email_5' => $columns[ 21 ], 'email_6' => $columns[ 22 ],
                                                    'web' => $columns[ 14 ] ) );

      $company_panel = array( 'panel' => $panel_name, 'survey' => $columns[ 0 ] );

      $company_marketing = array( 'scoring' => $columns[ 2 ] );

      $company_action_log = array( array( 'time' => current_time( 'mysql' ), 
                                          'by' => $current_email,
                                          'action' => "Creacion de empresa por Panel" ) );

      $company_query = array( "companyCUIT" => $columns[ 4 ],
                              "companyVenue" => 0,
                              "companyName" => $columns[ 5 ],
                              "companyStatus" => 1,
                              "companyFopStratum" => $columns[ 8 ],
                              "companyCIIU" => $columns[ 7 ],
                              "companyBranch" => $columns[ 3 ],
                              "companyEmployees" => $columns[ 6 ],
                              "companyRegion" => $columns[ 9 ],
                              "companySurvey" => json_encode( $company_panel ),
                              "companyMarketing" => json_encode( $company_marketing ),
                              "companyActionLog" => json_encode( $company_action_log ) );

      $wpdb->insert( "fop_panels_companies", $company_query );

      $company_id = $wpdb->get_var( "SELECT CompanyId FROM fop_panels_companies WHERE companyCUIT ='" . $columns[ 4 ] . "'" );
 
      $contact_query = array( "companyId" => $company_id,
                              "companyStreet" => $columns[ 13 ],
                              "companyProvince" => $columns[ 10 ],
                              "companyLocation" => $columns[ 11 ],
                              "companyZone" => $columns[ 12 ],
                              "companyStNumber" => 0, 
                              "companyFloor" => 0, 
                              "companyOffice" => 0, 
                              "companyPostCode" => 0, 
                              "companyContact" => json_encode( $company_contact ) );

      $wpdb->insert( "fop_panels_companies_contact", $contact_query );
   } else {
      // Company exists
    }
  }
  
  return true;
}

/*
* function:    companyExists
* Description: Checks if a CUIT is already loaded on Companies DB
* Parameters:  CUIT
* Returns:     CompanyId if exists, otherwise -1
*/
function companyExists( $cuit ) {
  global $wpdb;

  $result = $wpdb->get_var( "SELECT companyId FROM fop_panels_companies WHERE companyCUIT = " . $cuit );

  if( $result ) {
    return $result;
  }

  return -1;
}

/*
* function:    panelExists
* Description: Checks if a Panel of companies is already loaded on Panels DB
* Parameters:  Panel Name
* Returns:     panelId if exists, otherwise -1
*/
function panelExists( $panel_name ) {
  global $wpdb;

  $result = $wpdb->get_var( "SELECT panelId FROM fop_panels WHERE panelName = '" . $panel_name . "'" );

  if( $result ) {
    return $result;
  }

  return -1;
}

/*
* function:    generate_afip_csv
* Description: Generates CSV file containing data from AFIP ACtive Employeers
* Parameters:  None
* Returns:     None
*/
function generate_afip_csv() {
  global $wpdb;

  $rows = $wpdb->get_results( "SELECT * FROM fop_afip_companies ORDER BY afipCUIT");

  $filename = "/var/www/html/wp-content/uploads/fop/afip_active.csv";

  $header_row = array(
    'CUIT',
    'Razon Social',
    'Provincia',
    'Localidad',
    'Direccion',
    'Codigo Postal',
    'Latitud',
    'Longitud',
    'Alta FOP',
    'Baja FOP',
    'Actividades'
  );

  $data_rows = array();

  foreach( $rows as $row ) {
    $company = array( $row->afipCUIT,
                      $row->afipDenominacion,
                      $row->afipProvincia,
                      $row->afipLocalidad,
                      $row->afipDireccion,
                      $row->afipCodigoPostal,
                      $row->afipLatitud,
                      $row->afipLongitud,
                      $row->afipDesde,
                      $row->afipHasta );

    $list_activities = explode( "-", $row->afipActividades );
    foreach( $list_activities as $activity ) {
     $activity_query = "SELECT act_desc FROM fop_afip_activities WHERE id = " . $activity;
     $activity_desc = $wpdb->get_var( $activity_query );

     array_push( $company, $activity, $activity_desc );
    }

    $data_rows[] = $company;
  }

  $fp = fopen( $filename, 'w+');

  ob_clean();

  fputcsv( $fp, $header_row );
  foreach ( $data_rows as $data_row ) {
    fputcsv( $fp, $data_row );
  }

  fclose( $fp );
}

/*
* function:    notifyUserCreation
* Description: Notifies user creation to: New User, User Creator and System Administrator.
* Parameters:  Name, Email, Password, Creator email, Role
* Returns:     None
*/
function notifyUserCreation( $nombre, $email, $user_pass, $email_creator, $role ) {
  date_default_timezone_set( 'America/Argentina/Buenos_Aires' );
  $it_mail = "sistemas.fopyme@gmail.com";
 
  $msgUser  = "Hola " . $nombre. "! \r\n\r\nSe ha creado exitosamente tu usuario en la página de gestión de FOP.";
  $msgUser .= "\r\nDeberás acceder a https://gestion.observatoriopyme.org.ar e iniciar sesión.";
  $msgUser .= "\r\n\r\nDatos de sesión: ";
  $msgUser .= "\r\nUsuario: ". $email;
  $msgUser .= "\r\nContraseña: ". $user_pass;
  $msgUser .= "\r\n\r\nGracias!";
  $msgUser .= "\r\n\r\nDudas? Escribilas a ";
  $msgUser .= $it_mail;
 
  $msgCreator  = "Hola!\r\n\r\nCreaste el usuario ";
  $msgCreator .= $email;
  $msgCreator .= " en la plataforma de gestión de FOP.\r\nCreado el: ";
  $msgCreator .= current_time( 'mysql' );
  $msgCreator .= "\r\nEl rol del usuario es: ";
  $msgCreator .= $role;
  $msgCreator .= "\r\n\r\nGracias!";
  $msgCreator .= "\r\n\r\nDudas? Escribilas a ";
  $msgCreator .= $it_mail;

  $msgAdmin  = "Se ha creado el usuario: ";
  $msgAdmin .= $email;
  $msgAdmin .= "\r\nUsuario Creador: ";
  $msgAdmin .= $email_creator;
  $msgAdmin .= "\r\nCreado el: ";
  $msgAdmin .= current_time( 'mysql' );
  $msgAdmin .= "\r\nRol: ";
  $msgAdmin .= $role;

  wp_mail( $email, "Alta de Usuario FOP", $msgUser );
  wp_mail( $email_creator, "Creación de Usuario FOP", $msgCreator );
  wp_mail( $it_mail, "Creación de Usuario FOP", $msgAdmin );
}


?>

 
