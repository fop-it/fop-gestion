<?php get_header(); ?>

<div id="page-content" class="page-content">
  <div>
    <div style="height:140px" aria-hidden="true" class="wp-block-spacer">
    </div>

    <div style="text-align: center; ">
      <a href="<?php echo content_url( '/themes/one-page-express-child/inc/download-csv-file.php?f=usersDB&n=usuarios-activos' ) ?>">
      <input type="submit" class="fop-button" value="Descargar Activos">
      </a>

      <a href="<?php echo content_url( '/themes/one-page-express-child/inc/download-csv-file.php?f=deletedUsersDB&n=usuarios-eliminados' ) ?>">
      <input type="submit" class="fop-button" value="Descargar Eliminados">
      </a>

      <a href="/usuarios">
      <input type="submit" class="fop-button" value="Volver a Usuarios">
      </a>

      <br> <br> <br>
    </div>

    <div style="width: 1000px; text-align: center; margin-left: auto; margin-right:auto;" >
      <h6> Listado de Usuarios Activos </h6>
      <?php echo do_shortcode( '[table id=usersDB /]' ); ?>
    
      <br>
      <hr>
      <hr>

      <h6> Listado de Usuarios Eliminados </h6>
      <?php echo do_shortcode( '[table id=deletedUsers /]' ); ?>
    </div>

  </div>
</div>

<?php get_footer(); ?>

