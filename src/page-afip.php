<?php get_header(); ?>


<div id="page-content" class="page-content">
  <div>
    <div style="height:140px" aria-hidden="true" class="wp-block-spacer">
    </div>

    <div class="fop-form">
      <!-- Archivo AFIP Activos subido -->
      <?php if( isset( $_GET[ 'msg' ] ) && isset( $_GET[ 'f' ] ) ): ?>
        <div>
          <p style="color:green; font-weight:bold;"> <?php echo $_GET[ 'msg' ]; ?>  </p>
        </div>

        <h7>Procesar y Cargar CUITs Activos de AFIP</h7>

        <br>
          <form action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post" enctype="multipart/form-data">

           <label for="afipFile">Archivo:</label>
           <input type="text" id="afipFile" name="afipFile" readonly="readonly" value="<?php echo $_GET[ 'f' ]; ?>">

            <br>
            <input type="submit" class="fop-button" value="Procesar CUITs">

            <input type="hidden" name="action" value="processAfip">

          </form>

      <!-- Errores al procesar archivo AFIP -->
      <?php elseif( isset( $_GET[ 'afip' ] ) && isset( $_GET[ 'f' ] ) && isset( $_GET[ 'lc' ] ) && isset( $_GET[ 'emp' ] ) && isset( $_GET[ 'solo' ] ) && isset( $_GET[ 'nfound' ] ) ): ?>
        <div>
          <p style="color:orange; font-weight:bold;"> <?php echo $_GET[ 'afip' ]; ?>  </p>
        </div>

        <h7>Seguir con el procesamiento de CUITs Activos de AFIP</h7>

        <br>
          <form id="keepCuiting" action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post" enctype="multipart/form-data">

           <label for="afipFile">Archivo:</label>
           <input type="text" id="afipFile" name="afipFile" readonly="readonly" value="<?php echo $_GET[ 'f' ]; ?>">
           <input type="text" id="lastCuit" name="lastCuit" readonly="readonly" value="<?php echo $_GET[ 'lc' ]; ?>">
           <input type="text" id="emp" name="emp" readonly="readonly" value="<?php echo $_GET[ 'emp' ]; ?>">
           <input type="text" id="solo" name="solo" readonly="readonly" value="<?php echo $_GET[ 'solo' ]; ?>">
           <input type="text" id="nfound" name="nfound" readonly="readonly" value="<?php echo $_GET[ 'nfound' ]; ?>">

           <br>
           <input type="hidden" name="action" value="reprocessAfip">

          </form>

          <script type="text/javascript">
            document.getElementById( "keepCuiting" ).submit();
          </script>

      <!-- Cargamos empleadores en la tabla -->
      <?php elseif( isset( $_GET[ 'em' ] ) && isset( $_GET[ 'sl' ] ) && isset( $_GET[ 'nu' ] ) ): ?>
        <div>
          <h5>Se ha registrado un total de 
              <?php $employeers = $_GET[ 'em' ];
                    $solo       = $_GET[ 'sl' ];
                    $not_found  = $_GET[ 'nu' ];
                    $total      = $employeers + $solo; 
                    echo $total;
                    echo " CUITs activos.";
                    echo "<br>";
                    echo "<br>";
                    echo $employeers;
                    echo " empleadores.";
                    echo "<br>";
                    echo $solo;
                    echo " trabajadores independientes.";
                    echo "<br>";
                    echo $not_found;
                    echo " CUITs respondidos como inexistentes por AFIP."; ?>
          </h5>
        </div>

      <!-- Subir archivo ZIP AFIP completo -->
      <?php else: ?>
        <?php if( isset ( $_GET[ 'errormsg' ] ) ): ?>
          <div>
            <p style="color:red; font-weight:bold;"> <?php echo $_GET[ 'errormsg' ]; ?>  </p>
          </div>
        <?php endif; ?>

        <h5>Cargar archivo AFIP</h5>
        <h7>Formato aceptado: ZIP</h7><br>

        <a href="https://www.afip.gob.ar/genericos/cInscripcion/archivoCompleto.asp" target = "_blank">Registro AFIP SIN Denominación</a>
        <br>
          <form action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post" enctype="multipart/form-data">
            <br>
            <input type="file" name="afipFile" id="afipFile" accept=".zip">

            <br><br>
            <input type="submit" class="fop-button" value="CUITs Activos">

            <input type="hidden" name="action" value="uploadAfip">

          </form>
      <?php endif; ?>
    </div>

    <div style="text-align: center; ">
      <br> <br>
      <a href="/panel-de-control/">
       <input type="submit" class="fop-button" value="Panel de Control">
      </a>
      <br>
    </div>

  </div>
</div>

<?php get_footer(); ?>

