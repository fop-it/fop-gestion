<?php get_header(); ?>

<div id="page-content" class="page-content">
  <div>
    <div style="height:140px" aria-hidden="true" class="wp-block-spacer">
    </div>

    <div class="fop-form">
      <?php if( isset ( $_GET[ 'errormsg' ] ) ): ?>
        <div>
          <p style="color:red; font-weight:bold;"> <?php echo $_GET[ 'errormsg' ]; ?>  </p>
        </div>
      <?php endif; ?>
      <h5> Modifique su Contraseña </h5>

      <br>
      <form action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post">
        <label for="txtOldPwd">Antigua Contraseña: &nbsp;&nbsp; </label>
        <input type="password" id="txtOldPwd" name="txtOldPwd" style="width: 300px;">

        <br>
        <label for="txtNewPwd1">Nueva Contraseña:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label>
        <input type="password" id="txtNewPwd1" name="txtNewPwd1" style="width: 300px;">

        <br>
        <label for="txtNewPwd2">Repita la Contraseña: </label>
        <input type="password" id="txtNewPwd2" name="txtNewPwd2" style="width: 300px;">
 
        <br>
        <input type="submit" class="fop-button" value="Cambiar Contraseña" >

        <input type="hidden" name="action" value="modifyPwd">
      </form>
    </div>

  </div>
</div>

<?php get_footer(); ?>

