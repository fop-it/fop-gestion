<?php get_header(); ?>

include( 'inc/fop-utils.php' );


<div id="page-content" class="page-content">
  <div>
    <div style="height:140px" aria-hidden="true" class="wp-block-spacer">
    </div>

    <div class="fop-form">
      <?php if( isset ( $_GET[ 'd_id' ] ) &&
                isset ( $_GET[ 'd_name' ] ) &&
                isset ( $_GET[ 'd_email' ] ) &&
                isset ( $_GET[ 'd_role' ] ) ): ?>
          <h5> Confirme la eliminación del usuario </h5>

          <form action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post">
            <label for="txtId">ID: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
            <input type="text" id="txtId" name="txtId" style="width: 400px;" value="<?php echo $_GET[ 'd_id' ] ?>" readonly="readonly">

            <br>
            <label for="txtNombre">Nombre: </label>
            <input type="text" id="txtNombre" name="txtNombre" style="width: 400px;" value="<?php echo $_GET[ 'd_name' ] ?>" readonly="readonly">

            <br>
            <label for="txtEmail">Email:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label>
            <input type="text" id="txtEmail" name="txtEmail" style="width: 400px;" value="<?php echo $_GET[ 'd_email' ] ?>" readonly="readonly">

            <br>
            <label for="txtRole">Rol:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label>
            <input type="text" id="txtRole" name="txtRole" style="width: 400px;" value="<?php echo $_GET[ 'd_role' ] ?>" readonly="readonly">

            <input type="hidden" id="txtDate" name="txtDate" value="<?php echo $_GET[ 'd_date' ] ?>">

            <br>
            <br>
            <input type="submit" class="fop-button" value="Eliminar Usuario" >

            <input type="hidden" name="action" value="deleteUser">
         </form>
        </div>
      <?php else: ?>
        <?php if( isset ( $_GET[ 'errormsg']  ) ): ?>
          <div>
            <p style="color:red; font-weight:bold;"> <?php echo $_GET[ 'errormsg' ]; ?>  </p>
          </div>
        <?php endif; ?>
        <h5> Ingrese el email y busque el usuario a eliminar </h5>
        <h5> Sólo roles Encuestador/Comunicaciones/Reportes </h5>

        <form action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post">
        <br>
          <?php 
            $roles  = array( 'encuestador', 'marketing', 'reporte' );
            $emails = getUserEmailByRole( $roles );
          ?>

          <select name="txtEmail" style="width: 450px;">
          <option>Seleccione Email</option>
            <?php
              foreach( $emails as $email ) {
                echo '<option value="'.$email.'">'.$email.'</option>';
              }
            ?>
          </select>

          <br>
          <br>
          <input type="submit" class="fop-button" value="Buscar Usuario" >

          <input type="hidden" name="action" value="verifyUser" >

        </form>
      <?php endif; ?>
    </div>

    <div style="text-align: center; ">
      <br> <br>
      <a href="/usuarios">
      <input type="submit" class="fop-button" value="Volver a Usuarios">
      </a>
    </div>

  </div>
</div>

<?php get_footer(); ?>

