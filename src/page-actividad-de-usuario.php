<?php get_header(); ?>

include( 'inc/fop-utils.php' );


<div id="page-content" class="page-content">
  <div>
    <div style="height:140px" aria-hidden="true" class="wp-block-spacer">
    </div>

    <div class="fop-form">
      <?php if( isset ( $_GET[ 'errormsg' ] ) ): ?>
        <div>
          <p style="color:red; font-weight:bold;"> <?php echo $_GET[ 'errormsg' ]; ?>  </p>
        </div>
      <?php endif; ?>
      <h5> Descargue la actividad de un usuario </h5>
      <h5> Únicamente Roles Encuestador/Comunicaciones/Reportes </h5>

      <br>
      <form action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post">
        <br>
          <?php
            $roles  = array( 'encuestador', 'marketing', 'reporte' );
            $emails = getUserEmailByRole( $roles );
          ?>

          <select name="txtEmail" style="width: 450px;">
          <option>Seleccione Email</option>
            <?php
              foreach( $emails as $email ) {
                echo '<option value="' .$email. '">' .$email. '</option>';
              }
            ?>
          </select>

        <br>
        <h6> Ingrese la fecha a revisar: </h6>
        <label for="datePickerFrom">Desde:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label>
        <input type="text" id="datePickerFrom" name="datePickerFrom" style="width: 400px;">

        <br>
        <label for="datePickerTo">Hasta:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label>
        <input type="text" id="datePickerTo" name="datePickerTo" style="width: 400px;">

        <br>
        <input type="submit" class="fop-button" value="Descargar Actividad" >

        <input type="hidden" name="action" value="dlUserActivity">
      </form>
    </div>

    <div style="text-align: center; ">
      <br> <br>
      <a href="/reportes/">
       <input type="submit" class="fop-button" value="Volver a Reportes">
      </a>
      <br>
    </div>

  </div>
</div>

<?php get_footer(); ?>

