<?php

function captura_perfil() {
  if( empty( $_POST[ 'txtOldPwd' ] )  ||
      empty( $_POST[ 'txtNewPwd1' ] ) ||
      empty( $_POST[ 'txtNewPwd2' ] ) ):
    wp_redirect( add_query_arg( array( 'errormsg' => "ERROR: Todos los campos son OBLIGATORIOS." ),
                                       get_home_url() . '/perfil/') );
    exit;
  endif;

  $old_pwd   = $_POST[ 'txtOldPwd' ];
  $new_pwd_1 = $_POST[ 'txtNewPwd1' ];
  $new_pwd_2 = $_POST[ 'txtNewPwd2' ];

  global $wpdb;

  $user_id = get_current_user_id();
  $hashed = $wpdb->get_var( "SELECT user_pass FROM  wp_users WHERE  ID=" . $user_id );

  $checked = wp_check_password( $old_pwd, $hashed, $user_id );

  if( ! $checked ):
    wp_redirect( add_query_arg( array( 'errormsg' => "ERROR: La contraseña a cambiar ingresada no coincide. Reingrese los valores." ),
                                       get_home_url() . '/perfil/') );
    exit;
  endif;

  if( strcmp( $new_pwd_1, $new_pwd_2 ) !== 0 ):
    wp_redirect( add_query_arg( array( 'errormsg' => "ERROR: La nueva contraseña no coincide. Reingrese los valores." ),
                                       get_home_url() . '/perfil/') );
    exit;
  endif;

  wp_set_password( $new_pwd_1, $user_id );

  wp_redirect( get_home_url() );

  exit;
}

add_action( 'admin_post_modifyPwd', 'captura_perfil' );

?>

