<?php get_header(); ?>


<div id="page-content" class="page-content">
  <div>
    <div style="height:140px" aria-hidden="true" class="wp-block-spacer">
    </div>

    <div class="fop-form">
      <?php if( isset ( $_GET[ 'errormsg' ] ) ): ?>
        <div>
          <p style="color:red; font-weight:bold;"> <?php echo $_GET[ 'errormsg' ]; ?>  </p>
        </div>
      <?php endif; ?>

      <h7>Geolocalizar activos de AFIP</h7>
      <br>

      <form action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post" enctype="multipart/form-data">
      <br>

      <input type="submit" class="fop-button" value="Geolocalizar">

      <input type="hidden" name="action" value="geoAfip">

      </form>

      <br><br>
      <h7>Generar archivo KML</h7>
      <br>

      <form action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post" enctype="multipart/form-data">
      <br>

      <input type="submit" class="fop-button" value="Crear Mapa">

      <input type="hidden" name="action" value="geoMapa">

      </form>
    </div>

    <div style="text-align: center; ">
      <br> <br>
      <a href="/panel-de-control/">
       <input type="submit" class="fop-button" value="Panel de Control">
      </a>
      <br>
    </div>

  </div>
</div>

<?php get_footer(); ?>

