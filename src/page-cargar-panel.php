<?php get_header(); ?>

<?php
  global $wpdb;
  $paneles = $wpdb->get_results( "SELECT panelName FROM fop_panels" );
?>


<div id="page-content" class="page-content">
  <div>
    <div style="height:140px" aria-hidden="true" class="wp-block-spacer">
    </div>

    <div style="width: 100%; overflow: hidden;">
      <div style="float:left; width: 2%; color:white;"> . </div>
      <div style="width:30%; float: left;" class="fop-form">
        <?php if( isset ( $_GET[ 'errormsgpanel' ] ) ): ?>
          <div>
            <p style="color:red; font-weight:bold;"> <?php echo $_GET[ 'errormsgpanel' ]; ?>  </p>
          </div>
        <?php endif; ?>

        <h5>Suba el archivo de Panel</h5>
        <h7>Formatos aceptados: Excel ( xls | xlsx )</h7>
        <br>
        <h7>Corrobore las celdas antes de subirlo</h7>

       <br>
        <form action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post" enctype="multipart/form-data">
          <br>
          <label for="panelId"> Nombre del Panel (Por ej. EE2021H1): </label>
          <input type="text" id="panelId" name="panelId" style="width: 300px;">

          <br><br>
          <input type="file" name="panelFile" id="panelFile" accept=".xls,.xlsx">

          <br><br>
          <input type="submit" class="fop-button" value="Cargar Panel">

          <input type="hidden" name="action" value="uploadPanel">

        </form>

        <br>
        <a href="<?php echo content_url( '/themes/one-page-express-child/inc/download-excel-file.php?f=panelHeaderRow&t=modelo-panel' ) ?>">
        <input type="submit" value="Modelo de Panel">
        </a>

      </div>

      <div style="float:left; width: 3%; color:white;"> . </div>

      <div style="width:30%; float:left;" class="fop-form">
        <?php if( isset ( $_GET[ 'errormsgquota' ] ) ): ?>
          <div>
            <p style="color:red; font-weight:bold;"> <?php echo $_GET[ 'errormsgquota' ]; ?>  </p>
          </div>
        <?php endif; ?>

        <h5>Suba la cuota de encuestas</h5>
        <h7>Formatos aceptados: Excel ( xls | xlsx )</h7>
        <br>
        <h7>Corrobore las celdas antes de subirlo</h7>

        <br>
        <form action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post" enctype="multipart/form-data">
          <br>

          <label> Seleccione Panel: </label>
          <select name="txtQuota" style="width: 300px;">
            <option>...</option>
            <?php
              foreach( $paneles as $panel ) {
                echo '<option value="'.$panel->panelName.'">'.$panel->panelName.'</option>';
              }
            ?>
          </select>

          <br><br>
          <input type="file" name="quotaFile" id="quotaFile" accept=".xls,.xlsx">

          <br><br>
          <input type="submit" class="fop-button" value="Cargar Cuota">

          <input type="hidden" name="action" value="uploadQuota">
        </form>

        <br>
        <a href="<?php echo content_url( '/themes/one-page-express-child/inc/download-excel-file.php?f=panelQuota&t=modelo-cuota' ) ?>">
        <input type="submit" value="Modelo de Cuota">
        </a>

      </div>

      <div style="float:left; width: 3%; color:white;"> . </div>

      <div style="width:30%; float:left;" class="fop-form">
        <?php if( isset ( $_GET[ 'errormsgreassign' ] ) ): ?>
          <div>
            <p style="color:red; font-weight:bold;"> <?php echo $_GET[ 'errormsgreassign' ]; ?>  </p>
          </div>
        <?php endif; ?>

        <h5>Reasigne encuestas</h5>
        <h7>Formatos aceptados: Excel ( xls | xlsx )</h7>
        <br>
        <h7>Corrobore las celdas antes de subirlo</h7>

        <br>
        <form action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post" enctype="multipart/form-data">
          <br>

          <label> Seleccione Panel: </label>
          <select name="txtReassign" style="width: 250px;">
            <option>...</option>
            <?php
              foreach( $paneles as $panel ) {
                echo '<option value="'.$panel->panelName.'">'.$panel->panelName.'</option>';
              }
            ?>
          </select>

          <br><br>
          <input type="file" name="reassignFile" id="reassignFile" accept=".xls,.xlsx">

          <br><br>
          <input type="submit" class="fop-button" value="Reasignar">

          <input type="hidden" name="action" value="uploadReassign">
        </form>

        <br>
        <a href="<?php echo content_url( '/themes/one-page-express-child/inc/download-excel-file.php?f=panelReassign&t=modelo-reasignacion' ) ?>">
        <input type="submit" value="Modelo de Reasignación">
        </a>

      </div>

    </div>

    <div style="text-align: center; ">
      <br> <br>
      <a href="/universo-de-empresas/">
       <input type="submit" class="fop-button" value="Volver a Empresas">
      </a>
      <br>
    </div>

  </div>
</div>

<?php get_footer(); ?>

