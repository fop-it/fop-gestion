<?php

include_once 'fop-utils.php';


function captura_panel_agregar_usuario() {
  if( empty( $_POST[ 'txtNombre' ] )   || 
      empty( $_POST[ 'txtApellido' ] ) || 
      empty( $_POST[ 'txtEmail' ] )    || 
      !filter_var($_POST[ 'txtEmail' ], FILTER_VALIDATE_EMAIL ) ):
    wp_redirect( add_query_arg( array( 'errormsg' => "ERROR: Campos incompletos o inválidos. Reingrese los datos correctamente" ), 
                                get_home_url() . '/panel-agregar-usuario/') );
    exit;
  endif;

  $nombre   = ucwords( (sanitize_text_field( $_POST[ 'txtNombre' ] ) ), " " );
  $apellido = ucwords( (sanitize_text_field( $_POST[ 'txtApellido' ] ) ), " " );
  $email    = sanitize_text_field( $_POST[ 'txtEmail' ] );
  $role     = sanitize_text_field( $_POST[ 'radioRole' ] );

  if( checkUserExists( $email ) ) {
    wp_redirect( add_query_arg( array( 'errormsg' => "ERROR: El usuario que quiere crear ya existe. Revise los datos." ), 
                                get_home_url() . '/panel-agregar-usuario/') );
    exit;
  } else {
    if( !function_exists( 'wp_insert_user' ) ) { 
      require_once ABSPATH . WPINC . '/user.php'; 
    } 
  
    $user_pass = base64_encode( random_bytes( 6 ) );

    $userdata = array( 'user_pass' => $user_pass, 
                       'user_login' => $email, 
                       'user_nicename' => strtolower( $nombre.$apellido), 
                       'user_email' => $email, 
                       'first_name' => $nombre, 
                       'last_name' => $apellido,  
                       'show_admin_bar_front' => 'false', 
                       'role' => $role ); 
  
    $result = wp_insert_user( $userdata );

    if( is_wp_error( $result ) ) {
      $errors = $result->get_error_messages();
      foreach( $errors as $error ) {
        wp_redirect( add_query_arg( array( 'errormsg' => "ERROR: El usuario que quiere crear ya existe. Revise los datos." ), 
                                            get_home_url() . '/panel-agregar-usuario/') );
        exit;
      }
    }

    notifyUserCreation( $nombre, $email, $user_pass, wp_get_current_user()->user_email, $role );
    
    getUsersDbAsCSV();

    wp_redirect( get_home_url() . '/panel-de-control'); 

    exit;
  }
}

add_action('admin_post_addUserPanel', 'captura_panel_agregar_usuario');

?>

