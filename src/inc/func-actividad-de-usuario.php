<?php

include_once 'fop-utils.php';


function captura_reporte_actividad() {
  if( empty( $_POST[ 'txtEmail' ] )       ||
      empty( $_POST[ 'datePickerFrom' ] ) ||
      empty( $_POST[ 'datePickerTo' ] ) ):
    wp_redirect( add_query_arg( array( 'errormsg' => "ERROR: Campos incompletos. Reingrese los datos correctamente" ),
                                get_home_url() . '/actividad-de-usuario/') );
    exit;
  endif;

  if( !filter_var( $_POST[ 'txtEmail' ], FILTER_VALIDATE_EMAIL ) ):
    wp_redirect( add_query_arg( array( 'errormsg' => "ERROR: Email inválido. Reingrese los datos correctamente" ),
                                get_home_url() . '/actividad-de-usuario/') );
    exit;
  endif;

  $email          = sanitize_text_field( $_POST[ 'txtEmail' ] );
  $datePickerFrom = $_POST[ 'datePickerFrom' ];
  $datePickerTo   = $_POST[ 'datePickerTo' ];

  if( $user_id = getUserID( $email ) ) {
    $role = get_userdata( $user_id )->roles[0];

    if( strcmp( $role, "encuestador" ) && strcmp( $role, "marketing" ) && strcmp( $role, "reporte" ) ) {
      wp_redirect( add_query_arg( array( 'errormsg' => "ERROR: Reporte válido únicamente para roles Encuestador/Comunicaciones/Reportes" ),
                                  get_home_url() . '/actividad-de-usuario/') );
      exit;
    }

    $date_from = strtotime( $datePickerFrom );
    $date_to   = strtotime( $datePickerTo );

    if( $date_from > $date_to ) {
      wp_redirect( add_query_arg( array( 'errormsg' => "ERROR: Las fechas ingresadas son incorrectas" ),
                                  get_home_url() . '/actividad-de-usuario/') );
      exit;
    }

    downloadUserActivity( $user_id, $date_from, $date_to , $email );

    wp_redirect( get_home_url() . '/reportes/' );
    exit;
  } else {
    wp_redirect( add_query_arg( array( 'errormsg' => "ERROR: El usuario ingresado no existe. Revise los datos." ),
                                get_home_url() . '/actividad-de-usuario/') );
    exit;
  }
}

add_action('admin_post_dlUserActivity', 'captura_reporte_actividad');

?>

