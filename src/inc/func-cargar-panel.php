<?php

include_once( 'fop-utils.php' );


function captura_panel_empresas() {
  if( empty( $_POST[ 'panelId' ] ) ):
    wp_redirect( add_query_arg( array( 'errormsgpanel' => "ERROR: Ingrese un nombre para el Panel" ),
                                get_home_url() . '/cargar-panel/') );
    exit;
  endif;

  if( $_FILES[ 'panelFile' ][ 'name' ] ) {
    if( !function_exists( 'wp_handle_upload' ) ) {
      require_once( ABSPATH . 'wp-admin/includes/file.php' );
    }
    
    $uploaded_panel = wp_handle_upload( $_FILES[ 'panelFile' ], array( 'test_form' => false ) );
 
    if( $uploaded_panel && !isset( $uploaded_panel[ 'error' ] ) ) {
      if( !checkPanelFileHeader( $uploaded_panel[ 'file' ] ) ) {
        wp_delete_file( $uploaded_panel[ 'file' ] );
        wp_redirect( add_query_arg( array( 'errormsgpanel' => "ERROR: El archivo subido no corresponde a un panel" ),
                                            get_home_url() . '/cargar-panel/') );
        exit;
      }

      $success = loadPanelDB( $uploaded_panel[ 'file' ], $_POST[ 'panelId' ] );

      wp_delete_file( $uploaded_panel[ 'file' ] );

      if( !$success ) {
        wp_redirect( add_query_arg( array( 'errormsgpanel' => "ERROR: El nombre del panel a subir ya existe" ),
                                    get_home_url() . '/cargar-panel/') );
        exit;
      }
      wp_redirect( get_home_url() . '/universo-de-empresas/' );
      exit;
    } else {
      wp_redirect( add_query_arg( array( 'errormsgpanel' => $uploaded_panel[ 'error' ] ),
                                  get_home_url() . '/cargar-panel/') );
      exit;
    }
  } else {
    wp_redirect( add_query_arg( array( 'errormsgpanel' => "ERROR: No ha cargado ningún archivo" ),
                                get_home_url() . '/cargar-panel/') );
    exit;
  }
}

function captura_cuota_panel() {
  if( $_FILES[ 'quotaFile' ][ 'name' ] ) {
    if( !function_exists( 'wp_handle_upload' ) ) {
      require_once( ABSPATH . 'wp-admin/includes/file.php' );
    }

    $uploaded_quota = wp_handle_upload( $_FILES[ 'quotaFile' ], array( 'test_form' => false ) );

    if( $uploaded_quota && !isset( $uploaded_quota[ 'error' ] ) ) {
      if( !checkPanelQuotaHeader( $uploaded_quota[ 'file' ] ) ) {
        wp_delete_file( $uploaded_quota[ 'file' ] );
        wp_redirect( add_query_arg( array( 'errormsgquota' => "ERROR: El archivo subido no corresponde al formato" ),
                                            get_home_url() . '/cargar-panel/') );
        exit;
      }

      $success = loadQuotaDB( $uploaded_quota[ 'file' ], $_POST[ 'txtQuota' ] );

      wp_delete_file( $uploaded_quota[ 'file' ] );

      if( !$success ) {
        wp_redirect( add_query_arg( array( 'errormsgquota' => "ERROR: No se han podido cargar las cuotas correctamente" ),
                                    get_home_url() . '/cargar-panel/') );
        exit;
      }
      wp_redirect( get_home_url() . '/universo-de-empresas/' );
      exit;
    } else {
      wp_redirect( add_query_arg( array( 'errormsgquota' => $uploaded_quota[ 'error' ] ),
                                  get_home_url() . '/cargar-panel/') );
      exit;
    }
  } else {
    wp_redirect( add_query_arg( array( 'errormsgquota' => "ERROR: No ha cargado ningún archivo" ),
                                get_home_url() . '/cargar-panel/') );
    exit;
  }
}

function captura_reasigna_panel() {
  if( $_FILES[ 'reassignFile' ][ 'name' ] ) {
    if( !function_exists( 'wp_handle_upload' ) ) {
      require_once( ABSPATH . 'wp-admin/includes/file.php' );
    }

    $uploaded_reassign = wp_handle_upload( $_FILES[ 'reassignFile' ], array( 'test_form' => false ) );

    if( $uploaded_reassign && !isset( $uploaded_reassign[ 'error' ] ) ) {
      if( !checkReassignPanelHeader( $uploaded_reassign[ 'file' ] ) ) {
        wp_delete_file( $uploaded_reassign[ 'file' ] );
        wp_redirect( add_query_arg( array( 'errormsgreassign' => "ERROR: El archivo subido no corresponde al formato" ),
                                            get_home_url() . '/cargar-panel/') );
        exit;
      }

      $success = reassignSurveyFromPanel( $uploaded_reassign[ 'file' ], $_POST[ 'txtReassign' ] );

      wp_delete_file( $uploaded_reassign[ 'file' ] );

      if( !$success ) {
        wp_redirect( add_query_arg( array( 'errormsgreassign' => "ERROR: No se han podido cargar las reasignaciones correctamente " . $success ),
                                    get_home_url() . '/cargar-panel/') );
        exit;
      }
      wp_redirect( get_home_url() . '/universo-de-empresas/' );
      exit;
    } else {
      wp_redirect( add_query_arg( array( 'errormsgreassign' => $uploaded_reassign[ 'error' ] . $success ),
                                  get_home_url() . '/cargar-panel/') );
      exit;
    }
  } else {
    wp_redirect( add_query_arg( array( 'errormsgreassign' => "ERROR: No ha cargado ningún archivo" ),
                                get_home_url() . '/cargar-panel/') );
    exit;
  }            
}


add_action('admin_post_uploadPanel', 'captura_panel_empresas');
add_action('admin_post_uploadQuota', 'captura_cuota_panel');
add_action('admin_post_uploadReassign', 'captura_reasigna_panel');

?>

