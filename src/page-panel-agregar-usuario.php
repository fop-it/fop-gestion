<?php get_header(); ?>

<div id="page-content" class="page-content">
  <div>
    <div style="height:140px" aria-hidden="true" class="wp-block-spacer">
    </div>

    <div class="fop-form">
      <?php if( isset ( $_GET[ 'errormsg' ] ) ): ?>
        <div>
          <p style="color:red; font-weight:bold;"> <?php echo $_GET['errormsg']; ?>  </p>
        </div>
      <?php endif; ?>
      <h5> Complete los campos y presione Agregar </h5>

      <br>
      <form action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post">
        <label for="txtNombre">Nombre: </label>
        <input type="text" id="txtNombre" name="txtNombre" style="width: 400px;">
 
        <br>
        <label for="txtApellido">Apellido: </label>
        <input type="text" id="txtApellido" name="txtApellido" style="width: 400px;">

        <br>
        <label for="txtEmail">Email:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label>
        <input type="text" id="txtEmail" name="txtEmail" style="width: 400px;">

        <div align="left">
          <p>
            &nbsp;&nbsp;&nbsp;&nbsp;
            Rol:
            <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

            <input type="radio" id="director" name="radioRole" value="directorcampo">
            <label for="pollster">Director</label>
            <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

            <input type="radio" id="coordinador" name="radioRole" value="coordinador">
            <label for="pollster">Coordinador</label> 
            <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
            <input type="radio" id="supervisor" name="radioRole" value="supervisorcampo">
            <label for="pollster">Supervisor</label> &nbsp;&nbsp;&nbsp;&nbsp;
            <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

            <input type="radio" id="encuestador" name="radioRole" value="encuestador">
            <label for="pollster">Encuestador</label> &nbsp;&nbsp;&nbsp;&nbsp;
            <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        
            <input type="radio" id="marketing" name="radioRole" value="marketing">
            <label for="marketing">Comunicaciones</label>
            <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         
            <input type="radio" id="reporte" name="radioRole" value="reporte">
            <label for="marketing">Reportes</label>
          </p>
        </div>

        <br>
        <input type="submit" class="fop-button" value="Agregar Usuario" >

        <input type="hidden" name="action" value="addUserPanel">
      </form>
    </div>

    <div style="text-align: center; ">
      <br> <br>
      <a href="/panel-de-control">
       <input type="submit" class="fop-button" value="Panel">
      </a>
      <br> 
    </div>

  </div>
</div>

<?php get_footer(); ?>

