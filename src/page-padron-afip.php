<?php get_header(); ?>

<div id="page-content" class="page-content">
  <div>
    <div style="height:140px" aria-hidden="true" class="wp-block-spacer"></div>

    <div style="text-align: center; ">
      <a href="<?php echo content_url( '/themes/one-page-express-child/inc/download-afip-csv-file.php?f=afip_active&n=empleadores-activos' ) ?>">
      <input type="submit" class="fop-button" value="Empleadores">
      </a>
    </div>

    <!--
    </br></br></br>
    <table class="blueTable" style="width:600px" class="center">
      <thead>
        <tr>
          <th>Fecha de Actualización</th>
          <th>Total CUITs Activos</th>
          <th>Total de Empleadores</th>
        </tr>
      </thead>
      <tbody>
      <?php
        global $wpdb;
        $result = $wpdb->get_results( "SELECT * FROM fop_afip_updates" );

        foreach( $result as $row ) : ?>
          <tr>
            <td><?php echo $row->updateDate; ?></td>
            <td><?php echo $row->cuitTotal; ?></td>
            <td><?php echo $row->cuitEmpleadores; ?></td>
          </tr>
     <?php endforeach;?>
      </tbody>
    </table>
    -->

    </br></br></br>
    <table class="blueTable" style="width:800px" class="center">
      <thead>
        <tr>
          <th>Provincia</th>
          <th>Empleadores Activos</th>
          <th>Empleadores Baja</th>
          <th>Porcentaje</th>
        </tr>
      </thead>
      <tbody>
      <?php
        global $wpdb;

        $provincias = $wpdb->get_results( "SELECT * FROM fop_provinces" );
        $total = $wpdb->get_var( "SELECT COUNT(*) FROM fop_afip_companies WHERE afipActivo = 1" );
        $bajas = $wpdb->get_var( "SELECT COUNT(*) FROM fop_afip_companies WHERE afipActivo = 0" );

        foreach( $provincias as $provincia ) : ?>
	<tr>
            <td><?php echo $provincia->provDescription; ?></td>
            <td><?php echo $total_prov = $wpdb->get_var( "SELECT count(*) FROM fop_afip_companies " . 
                                                         "WHERE afipProvincia = '" . $provincia->provDescription . 
                                                         "' AND afipActivo = 1" ); ?></td>
            <td><?php echo $bajas_prov = $wpdb->get_var( "SELECT count(*) FROM fop_afip_companies " .
                                                         "WHERE afipProvincia = '" . $provincia->provDescription .
                                                         "' AND afipActivo = 0" ); ?></td>
            <td><?php echo round( ( $total_prov * 100 / $total ), 2 ) . "%"; ?></td>
          </tr>
      <?php endforeach;?>

        <tr>
          <td><?php echo "<b> TOTAL PAÍS </b>"; ?></td>
          <td><?php echo "<b>" . $total . "</b>"; ?></td>
          <td><?php echo "<b>" . $bajas . "</b>"; ?></td>
          <td><?php echo "<b> 100% </b>"; ?></td>
        </tr>
      </tbody>

    </table>

    <div style="text-align: center; ">
      <br> <br>
      <a href="/universo-de-empresas/">
       <input type="submit" class="fop-button" value="Volver a Empresas">
      </a>
      <br>
    </div>

  </div>
</div>

<?php get_footer(); ?>

